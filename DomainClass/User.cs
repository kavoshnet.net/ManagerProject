﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainClass
{
    public class User
    {
        public User()
        {
            this.Job_Govs = new HashSet<Job_Gov>();
            this.Job_NoGovs = new HashSet<Job_NoGov>();
            this.Job_Plans = new HashSet<Job_Plan>();
            this.Member_Moots = new HashSet<Member_Moot>();
            this.Edu_Recs = new HashSet<Edu_Rec>();
            this.Other_Skills = new HashSet<Other_Skill>();
            this.InServ_Trains = new HashSet<InServ_Train>();
            this.Job_Trains = new HashSet<Job_Train>();
            this.Pub_Trains = new HashSet<Pub_Train>();
            this.Res_Historys = new HashSet<Res_History>();
            this.Books = new HashSet<Book>();
            this.Articles = new HashSet<Article>();
            this.Cheers = new HashSet<Cheer>();
            this.Degrees = new HashSet<Degree>();
        }
        [Key]
        public Int64 ID { get; set; }

        [Required(ErrorMessage = "نام را وارد کنید.")]
        [Display(Name = "نام")]
        [StringLength(80)]
        public string Lname { get; set; }

        [Required(ErrorMessage = "نام خانوادگی را وارد کنید.")]
        [Display(Name = "نام خانوادگی")]
        [StringLength(80)]
        public string Fname { get; set; }

        [Required(ErrorMessage = "نام کاربری را وارد کنید.")]
        [Display(Name = "نام کاربری")]
        [StringLength(20)]
        public string UserName { get; set; }

        [Required(ErrorMessage = "کلمه عبور را وارد کنید.")]
        [Display(Name = "کلمه عبور")]
        [StringLength(255)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Display(Name = "تصویر کاربر", Prompt = "تصویر کاربر", Description = "تصویر کاربر")]
        //[Column(TypeName = "image")]
        public byte[] UserPic { get; set; }
        public Int64? RoleID { get; set; }
        public virtual Role Role { get; set; }
        public virtual P_Info P_Info { get; set; }
        public virtual E_Status E_Status { get; set; }
        public virtual ICollection<Job_Gov> Job_Govs { get; set; }
        public virtual ICollection<Job_NoGov> Job_NoGovs { get; set; }
        public virtual ICollection<Job_Plan> Job_Plans { get; set; }
        public virtual ICollection<Member_Moot> Member_Moots { get; set; }
        public virtual ICollection<Edu_Rec> Edu_Recs { get; set; }
        public virtual ICollection<Other_Skill> Other_Skills { get; set; }
        public virtual ICollection<InServ_Train> InServ_Trains { get; set; }
        public virtual ICollection<Job_Train> Job_Trains { get; set; }
        public virtual ICollection<Pub_Train> Pub_Trains { get; set; }
        public virtual ICollection<Res_History> Res_Historys { get; set; }
        public virtual ICollection<Book> Books { get; set; }
        public virtual ICollection<Article> Articles { get; set; }
        public virtual ICollection<Cheer> Cheers { get; set; }
        public virtual ICollection<Degree> Degrees { get; set; }
        public virtual Takmili Takmili { get; set; }

    }
}
