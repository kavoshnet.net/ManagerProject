﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MyClasses;
namespace DomainClass
{
    public class Job_NoGov
    {
        public Job_NoGov()
        {

        }
        [Key]
        public Int64 ID { get; set; }

        [Display(Name = "عنوان شغل یا سمت")]
        [StringLength(250)]
        public string Onvan_Job { get; set; }

        [Display(Name = "عنوان دستگاه")]
        [StringLength(250)]
        public string Dastgah { get; set; }

        [Display(Name = "واحد سازمانی")]
        [StringLength(250)]
        public string Vahed { get; set; }

        [Display(Name = "تاریخ شروع اشتغال")]
        [StringLength(10)]
        public string DateShoro { get; set; }

        [Display(Name = "تاریخ پایان اشتغال")]
        [StringLength(10)]
        public string DatePayan { get; set; }

        public Int64? UserID { get; set; }
        public virtual User User { get; set; }
    }
}
