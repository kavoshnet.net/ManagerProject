﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MyClasses;
namespace DomainClass
{
    public class Job_Plan
    {
        public Job_Plan()
        {
            
        }
        [Key]
        public Int64 ID { get; set; }

        [Display(Name = "عنوان طرح")]
        [StringLength(250)]
        public string Onvan_Plan { get; set; }

        [Display(Name = "نوع همکاری")]
        public NoHamkari? NoHamkari { get; set; }

        [Display(Name = "وضعیت طرح")]
        public VaziatTarh? VaziatTarh { get; set; }

        [Display(Name = "سال اجرا")]
        [StringLength(10)]
        public string DateEjra { get; set; }

        [Display(Name = "نتایج حاصله")]
        [StringLength(250)]
        public string Natije { get; set; }


        public Int64? UserID { get; set; }
        public virtual User User { get; set; }
    }
}
