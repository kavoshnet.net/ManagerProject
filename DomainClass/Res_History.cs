﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MyClasses;
namespace DomainClass
{
    public class Res_History
    {
        public Res_History()
        {

        }
        [Key]
        public Int64 ID { get; set; }

        [Display(Name = "عنوان پژوهش")]
        [StringLength(250)]
        public string Onvan_Res { get; set; }

        [Display(Name = "نوع پژوهش")]
        public NoPajohesh? NoPajohesh { get; set; }

        [Display(Name = "نام دستگاه تصویب کننده")]
        [StringLength(250)]
        public string Dastgah { get; set; }

        [Display(Name = "مقام تصویب کننده")]
        [StringLength(250)]
        public string Magham { get; set; }

        [Display(Name = "نوع همکاری")]
        public NoHamkariPajohesh? NoHamkariPajohesh { get; set; }

        [Display(Name = "تاریخ تصویب")]
        [StringLength(10)]
        public string DateTasvib { get; set; }

        [Display(Name = "مدت پژوهش")]
        public int? Modat { get; set; }


        public Int64? UserID { get; set; }
        public virtual User User { get; set; }
    }
}
