﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MyClasses;
namespace DomainClass
{
    public class E_Status
    {
        public E_Status()
        {

        }
        [Key]
        public Int64 ID { get; set; }

        [Display(Name = "شماره پرسنلی")]
        [StringLength(80)]
        public string Shp { get; set; }

        [Display(Name = "نوع استخدام")]
        public NoEstekhdam? NoEstekhdam { get; set; }

        [Display(Name = "نوع مقررات استخدامی")]
        public NoMoghararatEstekhdam? NoMoghararatEstekhdam { get; set; }

        [Display(Name = "وضعیت خدمت")]
        public VaziatKhedmat? VaziatKhedmat { get; set; }

        [Display(Name = "دستگاه محل خدمت")]
        [StringLength(250)]
        public string DastgahMahalKhedmat { get; set; }

        [Display(Name = "واحد سازمانی")]
        [StringLength(250)]
        public string VahedSazmani { get; set; }

        [Display(Name = "استان محل خدمت")]
        [StringLength(50)]
        public string G_Ostan { get; set; }

        [Display(Name = "شهرستان محل خدمت")]
        [StringLength(50)]
        public string G_Shahrestan { get; set; }

        [Display(Name = "بخش محل خدمت")]
        [StringLength(50)]
        public string G_Bakhsh { get; set; }
        
        [Display(Name = "دستگاه مامور کننده")]
        [StringLength(250)]
        public string DastgahMamor { get; set; }

        [Display(Name = "تاریخ شروع ماموریت")]
        [StringLength(10)]
        public string DateMamoriat { get; set; }

        [Display(Name = "تاریخ شروع به خدمت دولتی")]
        [StringLength(10)]
        public string DateShoroKhedmat { get; set; }

        [Display(Name = "تاریخ شروع به خدمت قراردادی")]
        [StringLength(10)]
        public string DateKhedmatGH { get; set; }

        [Display(Name = "شماره پست/عنوان پست سازمانی")]
        [StringLength(255)]
        public string ShPost { get; set; }

        [Display(Name = "عنوان رسته شغلی")]
        [StringLength(255)]
        public string OnvanRaste { get; set; }

        [Display(Name = "عنوان رشته شغلی")]
        [StringLength(250)]
        public string OnvanReshte { get; set; }

        [Display(Name = "طبقه شغلی")]
        public int? TabagheShoghli { get; set; }

        [Display(Name = "رتبه شغلی")]
        public RotbeShoghli? RotbeShoghli { get; set; }

        [Display(Name = "عنوان شغل فعلی")]
        [StringLength(250)]
        public string OnvanShoghl { get; set; }

        [Display(Name = "از تاریخ")]
        [StringLength(10)]
        public string FromDate { get; set; }

        [Display(Name = "پایه")]
        public int? Paye { get; set; }

        [Display(Name = "مرتبه")]
        public int? Martabe { get; set; }

        [Display(Name = "نمره ارزشیابی سال قبل")]
        public int? Nomre1Ghabl { get; set; }

        [Display(Name = "نمره ارزشیابی دو سال قبل")]
        public int? Nomre2Ghabl { get; set; }

        [Display(Name = "نمره ارزشیابی سه سال قیل")]
        public int? Nomre3Ghabl { get; set; }

        [Display(Name = "سال")]
        public int? T_Sal { get; set; }

        [Display(Name = "ماه")]
        public int? T_Mah { get; set; }

        [Display(Name = "تاریخ انتصاب در پست فعلی")]
        [StringLength(10)]
        public string DateEntesab { get; set; }

        [Display(Name = "سال")]
        public int? K_Sal { get; set; }

        [Display(Name = "ماه")]
        public int? K_Mah { get; set; }

        [Display(Name = "سال")]
        public int? G_Sal { get; set; }

        [Display(Name = "ماه")]
        public int? G_Mah { get; set; }

        [Display(Name = "سطح مدیریت فعلی")]
        public SathModiriat? SathModiriat { get; set; }

        public virtual User User { get; set; }
    }
}
