﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MyClasses;
namespace DomainClass
{
    public class Edu_Rec
    {
        public Edu_Rec()
        {

        }
        [Key]
        public Int64 ID { get; set; }

        [Display(Name = "مقطع تحصیلی")]
        public Maghta? Maghta { get; set; }

        [Display(Name = "رشته تحصیلی")]
        [StringLength(250)]
        public string Reshte { get; set; }

        [Display(Name = "تاریخ اخذ مدرک")]
        [StringLength(10)]
        public string Date_Akhz { get; set; }

        [Display(Name = "محل اخذ")]
        public MahalAkhz? MahalAkhz { get; set; }

        [Display(Name = "عنوان دانشگاه")]
        [StringLength(250)]
        public string OnvanDaneshgah { get; set; }

        [Display(Name = "کشور")]
        [StringLength(80)]
        public string Keshvar { get; set; }

        [Display(Name = "استان")]
        [StringLength(80)]
        public string Ostan { get; set; }

        [Display(Name = "شهرستان")]
        [StringLength(80)]
        public string Shahrestan { get; set; }

        [Display(Name = "شهر")]
        [StringLength(80)]
        public string Shahr { get; set; }

        public Int64? UserID { get; set; }
        public virtual User User { get; set; }
    }
}
