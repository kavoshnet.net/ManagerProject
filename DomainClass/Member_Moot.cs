﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MyClasses;
namespace DomainClass
{
    public class Member_Moot
    {
        public Member_Moot()
        {

        }
        [Key]
        public Int64 ID { get; set; }

        [Display(Name = "عنوان شورا")]
        [StringLength(250)]
        public string Onvan_Shora { get; set; }

        [Display(Name = "عنوان دستگاه")]
        [StringLength(250)]
        public string Onvan_Dastgah { get; set; }

        [Display(Name = "سمت یا نوع همکاری")]
        public NoHamkariMoot? NoHamkari_Moot { get; set; }

        [Display(Name = "تاریخ شروع عضویت")]
        [StringLength(10)]
        public string Date_Shoro { get; set; }

        [Display(Name = "تاریخ خاتمه عضویت")]
        [StringLength(10)]
        public string Date_khateme { get; set; }


        public Int64? UserID { get; set; }
        public virtual User User { get; set; }
    }
}
