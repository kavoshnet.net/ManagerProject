﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClass.ViewModel
{
    public class Job_GovResources
    {
        public Int64 ID { get; set; }

        [Display(Name = "عنوان شغل یا سمت")]
        [StringLength(250)]
        public string Onvan_Job { get; set; }

        [Display(Name = "دستگاه محل خدمت")]
        [StringLength(250)]
        public string Dastgah { get; set; }

        [Display(Name = "واحد سازمانی")]
        [StringLength(250)]
        public string Vahed { get; set; }

        [Display(Name = "تاریخ انتصاب از")]
        [StringLength(10)]
        public string DateEntesabFrom { get; set; }

        [Display(Name = "تاریخ انتصاب تا")]
        [StringLength(10)]
        public string DateEntesabTo { get; set; }
        [Display(Name = "کد کاربر")]
        public Int64? UserID { get; set; }


    }
}
