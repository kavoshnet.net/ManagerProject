﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClass.ViewModel
{
    public class Account
    {
        public string UserName { get; set; }

        [Required(ErrorMessage = "کلمه عبور را وارد کنید.")]
        [Display(Name = "کلمه عبور")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "یادآوری")]
        public bool RememberMe { get; set; }
    }
}
