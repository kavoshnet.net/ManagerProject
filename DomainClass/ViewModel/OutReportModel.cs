﻿using MyClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClass.ViewModel
{
    public class OutReportModel
    {
        [Display(Name = "کد")]
        public Int64 ID { get; set; }
        [Display(Name = "کد کاربر")]
        public Int64 UserID { get; set; }
        [Display(Name = "نام")]
        public string Lname { get; set; }
        [Display(Name = "نام خانوادگی")]
        public string Fname { get; set; }
        [Display(Name = "تاریخ تولد")]
        public string BDate { get; set; }
        [Display(Name = "شماره ملی")]
        public string ShMeli { get; set; }
        [Display(Name = "جنسیت")]
        public string Jensiat { get; set; }
        [Display(Name = "مقطع تحصیلی")]
        public string Maghta { get; set; }


        //------------------------ ایثارگری
        [Display(Name = "رزمنده")]
        public bool Razmandeh { get; set; }
        [Display(Name = "مدت حضور در جبهه")]
        public int? ModatJebhe { get; set; }
        [Display(Name = "آزاده")]
        public bool Azadeh { get; set; }
        [Display(Name = "مدت حضور غیر داوطلبانه در جبه")]
        public int? ModatJebheGh { get; set; }
        [Display(Name = "جانباز")]
        public bool Janbaz { get; set; }
        [Display(Name = "میزان جانبازی")]
        public int? MizanJanbazi { get; set; }
        [Display(Name = "مدت اسارت")]
        public int? ModatEsarat { get; set; }
        [Display(Name = "خانواده شهید")]
        public bool Shahid { get; set; }
        [Display(Name = "تعداد شهدای خانواده")]
        public int? TedadShahid { get; set; }
        [Display(Name = "بسیجی فعال")]
        public int? BasijFal { get; set; }
        [Display(Name = "نسبت با شهید")]
        public string NesbatShahid { get; set; }
        //--------------------------------------------
        //----------------------پست سازمانی 
        [Display(Name = "شماره پست/عنوان پست سازمانی")]
        public string ShPost { get; set; }
        [Display(Name = "عنوان رسته شغلی")]
        public string OnvanRaste { get; set; }
        [Display(Name = "عنوان رشته شغلی")]
        public string OnvanReshte { get; set; }
        //--------------------------------------------
        //-------------سابقه خدمت 
        [Display(Name = "سال")]
        public int? K_Sal { get; set; }
        [Display(Name = "ماه")]
        public int? K_Mah { get; set; }
        //--------------------------------------------
        [Display(Name = "نوع استخدام")]
        public string NoEstekhdam { get; set; }
        //[Display(Name = "سن")]
        //public string Sen { get; set; }
        [Display(Name = "سابقه مدیریتی")]
        public List<Job_GovResources> Job_Govs { get; set; }
        [Display(Name = "نمره ارزشیابی سال قیل")]
        public int? Nomre1Ghabl { get; set; }
        [Display(Name = "نمره ارزشیابی 2سال قیل")]
        public int? Nomre2Ghabl { get; set; }
        [Display(Name = "نمره ارزشیابی 3سال قیل")]
        public int? Nomre3Ghabl { get; set; }

        //[Required(ErrorMessage = "کلمه عبور را وارد کنید.")]
        //[Display(Name = "کلمه عبور")]
        //[DataType(DataType.Password)]
        //public string Password { get; set; }

        //[Display(Name = "یادآوری")]
        //public bool RememberMe { get; set; }

    }
}
