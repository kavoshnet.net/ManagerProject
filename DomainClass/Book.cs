﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MyClasses;
namespace DomainClass
{
    public class Book
    {
        public Book()
        {

        }
        [Key]
        public Int64 ID { get; set; }

        [Display(Name = "عنوان اثر")]
        [StringLength(250)]
        public string Onvan_Book { get; set; }

        [Display(Name = "نوع اثر")]
        public NoAsar? NoAsar { get; set; }

        [Display(Name = "ناشر")]
        [StringLength(250)]
        public string Nasher { get; set; }

        [Display(Name = "سال نشر")]
        public int? SaleNashr { get; set; }

        public Int64? UserID { get; set; }
        public virtual User User { get; set; }
    }
}
