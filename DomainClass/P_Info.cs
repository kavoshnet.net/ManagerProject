﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MyClasses;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using System.ComponentModel;

namespace DomainClass
{
    public class P_Info
    {
        public P_Info()
        {
            Jensiat = MyClasses.Jensiat.mard;

        }
        [Key]
        public Int64 ID { get; set; }

        [Required(ErrorMessage = "نام را وارد کنید.")]
        [Display(Name = "نام")]
        [StringLength(80)]
        public string Lname { get; set; }

        [Required(ErrorMessage = "نام خانوادگی را وارد کنید.")]
        [Display(Name = "نام خانوادگی")]
        [StringLength(80)]
        public string Fname { get; set; }

        //[Required(ErrorMessage = "شماره ملی را وارد کنید.")]
        [Display(Name = "شماره ملی")]
        [StringLength(80)]
        public string ShMeli { get; set; }

        [Required(ErrorMessage = "نام پدر را وارد کنید.")]
        [Display(Name = "نام پدر")]
        [StringLength(80)]
        public string FaName { get; set; }

        [Required(ErrorMessage = "تاریخ تولد را وارد کنید.")]
        [Display(Name = "تاریخ تولد")]
        [StringLength(10)]
        public string BDate { get; set; }

        [Display(Name = "استان محل تولد")]
        [StringLength(50)]
        public string T_Ostan { get; set; }

        [Display(Name = "شهرستان محل تولد")]
        [StringLength(50)]
        public string T_Shahrestan { get; set; }

        [Display(Name = "بخش محل تولد")]
        [StringLength(50)]
        public string T_Bakhsh { get; set; }

        [Display(Name = "شماره شناسنامه")]
        [StringLength(50)]
        public string ShSh { get; set; }
        [Display(Name = "استان محل صدور")]
        [StringLength(50)]
        public string S_Ostan { get; set; }

        [Display(Name = "شهرستان محل صدور")]
        [StringLength(50)]
        public string S_Shahrestan { get; set; }

        [Display(Name = "بخش محل صدور")]
        [StringLength(50)]
        public string S_Bakhsh { get; set; }

        //[Required(ErrorMessage = "جنسیت را وارد کنید.")]
        [Display(Name = "جنسیت")]
        public Jensiat? Jensiat { get; set; }

        [Required(ErrorMessage = "دین را وارد کنید.")]
        [Display(Name = "دین")]
        [StringLength(50)]
        public string Din { get; set; }

        //[Required(ErrorMessage = "تاهل را وارد کنید.")]
        [Display(Name = "وضعیت تاهل")]
        public Tahol? Tahol { get; set; }

        [Display(Name = "آدرس پست الکترونیک")]
        [StringLength(250)]
        public string Email { get; set; }

        [Display(Name = "نمابر")]
        [StringLength(20)]
        public string Fax { get; set; }

        [Display(Name = "تلفن همراه")]
        [StringLength(11)]
        public string Mobile { get; set; }

        [Display(Name = "تلفن ثابت")]
        [StringLength(10)]
        public string Tell { get; set; }

        [Display(Name = "کد شهرستان")]
        [StringLength(10)]
        public string Code { get; set; }

        //[Required(ErrorMessage = "وضعیت نظام وظیفه را وارد کنید.")]
        [Display(Name = "وضعیت نظام وظیفه")]
        public NezamVazife? NezamVazife { get; set; }

        [Display(Name = "رزمنده")]
        public Boolean Razmandeh { get; set; }

        [Display(Name = "مدت حضور در جبهه")]
        public int? ModatJebhe { get; set; }

        [Display(Name = "آزاده")]
        public Boolean Azadeh { get; set; }

        [Display(Name = "مدت حضور غیر داوطلبانه در جبهه")]
        public int? ModatJebheGh { get; set; }

        [Display(Name = "جانباز")]
        public Boolean Janbaz { get; set; }

        [Display(Name = "میزان جانبازی")]
        public int? MizanJanbazi { get; set; }

        [Display(Name = "مدت اسارت")]
        public int? ModatEsarat { get; set; }

        [Display(Name = "خانواده شهید")]
        public Boolean Shahid { get; set; }

        [Display(Name = "تعداد شهدای خانواده")]
        public int? TedadShahid { get; set; }

        [Display(Name = "بسیجی فعال")]
        public int? BasijFal { get; set; }

        [Display(Name = "نسبت با شهید")]
        public NesbatShahid? NesbatShahid { get; set; }

        public virtual User User { get; set; }
    }
}
