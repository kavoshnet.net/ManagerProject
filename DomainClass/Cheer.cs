﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MyClasses;
namespace DomainClass
{
    public class Cheer
    {
        public Cheer()
        {

        }
        [Key]
        public Int64 ID { get; set; }

        [Display(Name = "دستگاه اجرایی")]
        [StringLength(250)]
        public string Dastgah_Cheer { get; set; }

        [Display(Name = "مقام تشویق کننده")]
        [StringLength(250)]
        public string Magham_Cheer { get; set; }

        [Display(Name = "سطح تشویق")]
        public SathTashvigh? SathTashvigh { get; set; }

        [Display(Name = "تاریخ تشویق")]
        [StringLength(10)]
        public string Date_Cheer { get; set; }

        [Display(Name = "علت تشویق")]
        [StringLength(250)]
        public string Elat_Cheer { get; set; }


        public Int64? UserID { get; set; }
        public virtual User User { get; set; }
    }
}
