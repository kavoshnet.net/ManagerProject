﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MyClasses;
namespace DomainClass
{
    public class Degree
    {
        public Degree()
        {

        }
        [Key]
        public Int64 ID { get; set; }

        [Display(Name = "عنوان مدال")]
        [StringLength(250)]
        public string Onvan_Deg { get; set; }

        [Display(Name = "نوع تقدیر")]
        public NoTaghdir? NoTaghdir { get; set; }

        [Display(Name = "مقام اعطا کننده")]
        [StringLength(250)]
        public string Magham_Deg { get; set; }

        [Display(Name = "تاریخ اخذ")]
        [StringLength(10)]
        public string Date_Deg { get; set; }

        [Display(Name = "علت اعطاء")]
        [StringLength(250)]
        public string Elat_Deg { get; set; }


        public Int64? UserID { get; set; }
        public virtual User User { get; set; }
    }
}
