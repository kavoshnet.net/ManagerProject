﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MyClasses;
namespace DomainClass
{
    public class Article
    {
        public Article()
        {

        }
        [Key]
        public Int64 ID { get; set; }

        [Display(Name = "عنوان مقاله")]
        [StringLength(250)]
        public string Onvan_Article { get; set; }

        [Display(Name = "نوع مقاله")]
        public NoMaghale? NoMaghale { get; set; }

        [Display(Name = "ناشر")]
        [StringLength(250)]
        public string Nasher { get; set; }

        [Display(Name = "سال نشر")]
        public int? SaleNashr { get; set; }

        public Int64? UserID { get; set; }
        public virtual User User { get; set; }
    }
}
