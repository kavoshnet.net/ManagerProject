﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MyClasses;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainClass
{
    public class Takmili
    {
        public Takmili()
        {

        }
        [Key]
        public Int64 ID { get; set; }

        [Display(Name = "تحصیلات حوزوی")]
        public TahsilatHozavi? TahsilatHozavi { get; set; }

        [Display(Name = "فنی و مهندسی")]
        public Boolean Fani { get; set; }

        [Display(Name = "امور اجتماعی")]
        public Boolean Ejtemae { get; set; }

        [Display(Name = "آموزشی و فرهنگی")]
        public Boolean Farhangi { get; set; }

        [Display(Name = " اداری و مالی")]
        public Boolean Edari { get; set; }

        [Display(Name = "فناوری اطلاعات")]
        public Boolean Fanavari { get; set; }

        [Display(Name = "سایر موارد")]
        public Boolean Sayer { get; set; }


        [Display(Name = "توضیحات سایر موارد")]
        [Column(TypeName = "ntext")]
        public string Description { get; set; }

        [Display(Name = "سیاست سازمان")]
        [Column(TypeName = "ntext")]
        public string Siasat { get; set; }

        [Display(Name = "اقدامات")]
        [Column(TypeName = "ntext")]
        public string Eghdamat { get; set; }

        [Display(Name = "دخالت")]
        [Column(TypeName = "ntext")]
        public string Dekhalat { get; set; }

        [Display(Name = "همکاری داخلی")]
        [Column(TypeName = "ntext")]
        public string HamkariDakheli { get; set; }

        [Display(Name = "همکاری خارجی")]
        [Column(TypeName = "ntext")]
        public string HamkariKhareji { get; set; }

        [Display(Name = "همکاری مختلف")]
        [Column(TypeName = "ntext")]
        public string HamkariMokhtalef { get; set; }

        [Display(Name = "دانش علمی")]
        public Boolean DaneshElmi { get; set; }

        [Display(Name = "ایجاد خلاقیت")]
        [Column(TypeName = "ntext")]
        public string IjadeKhalaghiat { get; set; }

        [Display(Name = "سبک مدیریت")]
        [Column(TypeName = "ntext")]
        public string SabkeModiriat { get; set; }

        [Display(Name = "قانون مدیریت خدمات کشوری")]
        public int? GH_Modiriat { get; set; }

        [Display(Name = "قانون محاسبات عمومی")]
        public int? GH_Mohasebat { get; set; }

        [Display(Name = "قانون الحاقی بخشی از تنظیم قوانین و مقررات")]
        public int? GH_Elhagi { get; set; }

        [Display(Name = "قانون تخلفات اداری")]
        public int? GH_Takhalofat { get; set; }

        [Display(Name = "سایر قوانین و مقررات")]
        public int? GH_Sayer { get; set; }

        [Display(Name = "برنامه ششم")]
        public int? GH_SheShom { get; set; }

        [Display(Name = "توضیحات ضروری")]
        [Column(TypeName = "ntext")]
        public string Tozihat_Zarori { get; set; }

        [Display(Name = "تاریخ تکمیل")]
        [StringLength(10)]
        public string Date_Takmil { get; set; }

        [Display(Name = "نام و نام خانوادگی مسئول امور اداری")]
        [StringLength(250)]
        public string LFName_Masol { get; set; }

        public virtual User User { get; set; }
    }
}
