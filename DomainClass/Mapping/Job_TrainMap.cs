using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class Job_TrainMap : EntityTypeConfiguration<Job_Train>
    {
        public Job_TrainMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            //this.Property(t => t.Lname)
            //    .IsRequired();




            // Table & Column Mappings
            this.ToTable("Job_Trains");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Onvan_Train).HasColumnName("Onvan_Train");
            this.Property(t => t.Dastgah_Train).HasColumnName("Dastgah_Train");
            this.Property(t => t.Date_Train).HasColumnName("Date_Train");
            this.Property(t => t.Modat_Train).HasColumnName("Modat_Train");
            this.Property(t => t.Mahal_Train).HasColumnName("Mahal_Train");
            this.Property(t => t.UserID).HasColumnName("UserID");
            // Relationships
            this.HasOptional(t => t.User)
                .WithMany(t => t.Job_Trains)
                .HasForeignKey(d => d.UserID);

        }
    }
}
