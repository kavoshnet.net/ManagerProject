using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class CheerMap : EntityTypeConfiguration<Cheer>
    {
        public CheerMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            //this.Property(t => t.Lname)
            //    .IsRequired();




            // Table & Column Mappings
            this.ToTable("Cheers");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Dastgah_Cheer).HasColumnName("Dastgah_Cheer");
            this.Property(t => t.Magham_Cheer).HasColumnName("Magham_Cheer");
            this.Property(t => t.SathTashvigh).HasColumnName("SathTashvigh");
            this.Property(t => t.Dastgah_Cheer).HasColumnName("Dastgah_Cheer");
            this.Property(t => t.Elat_Cheer).HasColumnName("Elat_Cheer");
            this.Property(t => t.UserID).HasColumnName("UserID");
            // Relationships
            this.HasOptional(t => t.User)
                .WithMany(t => t.Cheers)
                .HasForeignKey(d => d.UserID);

        }
    }
}
