using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class Res_HistoryMap : EntityTypeConfiguration<Res_History>
    {
        public Res_HistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            //this.Property(t => t.Lname)
            //    .IsRequired();




            // Table & Column Mappings
            this.ToTable("Res_Historys");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Onvan_Res).HasColumnName("Onvan_Res");
            this.Property(t => t.NoPajohesh).HasColumnName("NoPajohesh");
            this.Property(t => t.Dastgah).HasColumnName("Dastgah");
            this.Property(t => t.Magham).HasColumnName("Magham");
            this.Property(t => t.NoHamkariPajohesh).HasColumnName("NoHamkariPajohesh");
            this.Property(t => t.DateTasvib).HasColumnName("DateTasvib");
            this.Property(t => t.Modat).HasColumnName("Modat");
            this.Property(t => t.UserID).HasColumnName("UserID");
            // Relationships
            this.HasOptional(t => t.User)
                .WithMany(t => t.Res_Historys)
                .HasForeignKey(d => d.UserID);

        }
    }
}
