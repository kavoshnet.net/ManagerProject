using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class E_StatusMap : EntityTypeConfiguration<E_Status>
    {
        public E_StatusMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            //this.Property(t => t.Shp)
            //    .IsRequired();




            // Table & Column Mappings
            this.ToTable("E_Statuses");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Shp).HasColumnName("Shp");
            this.Property(t => t.NoEstekhdam).HasColumnName("NoEstekhdam");
            this.Property(t => t.NoMoghararatEstekhdam).HasColumnName("NoMoghararatEstekhdam");
            this.Property(t => t.VaziatKhedmat).HasColumnName("VaziatKhedmat");
            this.Property(t => t.DastgahMahalKhedmat).HasColumnName("DastgahMahalKhedmat");
            this.Property(t => t.VahedSazmani).HasColumnName("VahedSazmani");
            this.Property(t => t.G_Ostan).HasColumnName("G_Ostan");
            this.Property(t => t.G_Shahrestan).HasColumnName("G_Shahrestan");
            this.Property(t => t.G_Bakhsh).HasColumnName("G_Bakhsh");
            this.Property(t => t.DastgahMamor).HasColumnName("DastgahMamor");
            this.Property(t => t.DateMamoriat).HasColumnName("DateMamoriat");
            this.Property(t => t.DateShoroKhedmat).HasColumnName("DateShoroKhedmat");
            this.Property(t => t.DateKhedmatGH).HasColumnName("DateKhedmatGH");
            this.Property(t => t.ShPost).HasColumnName("ShPost");
            this.Property(t => t.OnvanRaste).HasColumnName("OnvanRaste");
            this.Property(t => t.OnvanReshte).HasColumnName("OnvanReshte");
            this.Property(t => t.TabagheShoghli).HasColumnName("TabagheShoghli");
            this.Property(t => t.RotbeShoghli).HasColumnName("RotbeShoghli");
            this.Property(t => t.OnvanShoghl).HasColumnName("OnvanShoghl");
            this.Property(t => t.FromDate).HasColumnName("FromDate");
            this.Property(t => t.Paye).HasColumnName("Paye");
            this.Property(t => t.Martabe).HasColumnName("Martabe");
            this.Property(t => t.Nomre1Ghabl).HasColumnName("Nomre1Ghabl");
            this.Property(t => t.Nomre2Ghabl).HasColumnName("Nomre2Ghabl");
            this.Property(t => t.Nomre3Ghabl).HasColumnName("Nomre3Ghabl");
            this.Property(t => t.T_Sal).HasColumnName("T_Sal");
            this.Property(t => t.T_Mah).HasColumnName("T_Mah");
            this.Property(t => t.DateEntesab).HasColumnName("DateEntesab");
            this.Property(t => t.K_Sal).HasColumnName("K_Sal");
            this.Property(t => t.K_Mah).HasColumnName("K_Mah");
            this.Property(t => t.G_Sal).HasColumnName("G_Sal");
            this.Property(t => t.G_Mah).HasColumnName("G_Mah");
            this.Property(t => t.SathModiriat).HasColumnName("SathModiriat");
            // Relationships
            this.HasRequired(t => t.User).
                WithOptional(t=> t.E_Status);

        }
    }
}
