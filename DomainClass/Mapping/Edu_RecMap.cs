using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class Edu_RecMap : EntityTypeConfiguration<Edu_Rec>
    {
        public Edu_RecMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            //this.Property(t => t.Lname)
            //    .IsRequired();




            // Table & Column Mappings
            this.ToTable("Edu_Recs");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Maghta).HasColumnName("Maghta");
            this.Property(t => t.Reshte).HasColumnName("Reshte");
            this.Property(t => t.Date_Akhz).HasColumnName("Date_Akhz");
            this.Property(t => t.MahalAkhz).HasColumnName("MahalAkhz");
            this.Property(t => t.OnvanDaneshgah).HasColumnName("OnvanDaneshgah");
            this.Property(t => t.Keshvar).HasColumnName("Keshvar");
            this.Property(t => t.Ostan).HasColumnName("Ostan");
            this.Property(t => t.Shahrestan).HasColumnName("Shahrestan");
            this.Property(t => t.Shahr).HasColumnName("Shahr");

            this.Property(t => t.UserID).HasColumnName("UserID");
            // Relationships
            this.HasOptional(t => t.User)
                .WithMany(t => t.Edu_Recs)
                .HasForeignKey(d => d.UserID);

        }
    }
}
