using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class Job_PlanMap : EntityTypeConfiguration<Job_Plan>
    {
        public Job_PlanMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            //this.Property(t => t.Lname)
            //    .IsRequired();




            // Table & Column Mappings
            this.ToTable("Job_Plans");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Onvan_Plan).HasColumnName("Onvan_Plan");
            this.Property(t => t.NoHamkari).HasColumnName("NoHamkari");
            this.Property(t => t.VaziatTarh).HasColumnName("VaziatTarh");
            this.Property(t => t.DateEjra).HasColumnName("DateEjra");
            this.Property(t => t.Natije).HasColumnName("Natije");
            this.Property(t => t.UserID).HasColumnName("UserID");
            // Relationships
            this.HasOptional(t => t.User)
                .WithMany(t => t.Job_Plans)
                .HasForeignKey(d => d.UserID);

        }
    }
}
