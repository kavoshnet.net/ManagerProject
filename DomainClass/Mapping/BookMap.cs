using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class BookMap : EntityTypeConfiguration<Book>
    {
        public BookMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            //this.Property(t => t.Lname)
            //    .IsRequired();




            // Table & Column Mappings
            this.ToTable("Books");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Onvan_Book).HasColumnName("Onvan_Book");
            this.Property(t => t.NoAsar).HasColumnName("NoAsar");
            this.Property(t => t.Nasher).HasColumnName("Nasher");
            this.Property(t => t.SaleNashr).HasColumnName("SaleNashr");
            this.Property(t => t.UserID).HasColumnName("UserID");
            // Relationships
            this.HasOptional(t => t.User)
                .WithMany(t => t.Books)
                .HasForeignKey(d => d.UserID);

        }
    }
}
