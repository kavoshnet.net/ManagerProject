using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class P_InfoMap : EntityTypeConfiguration<P_Info>
    {
        public P_InfoMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Lname)
                .IsRequired();
            this.Property(t => t.FaName)
                .IsRequired();
            this.Property(t => t.Fname)
                .IsRequired();


            // Table & Column Mappings
            this.ToTable("P_Infos");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Lname).HasColumnName("Lname");
            this.Property(t => t.Fname).HasColumnName("Fname");
            this.Property(t => t.FaName).HasColumnName("FaName");
            this.Property(t => t.BDate).HasColumnName("BDate");
            this.Property(t => t.T_Ostan).HasColumnName("T_Ostan");
            this.Property(t => t.T_Shahrestan).HasColumnName("T_Shahrestan");
            this.Property(t => t.T_Bakhsh).HasColumnName("T_Bakhsh");
            this.Property(t => t.S_Ostan).HasColumnName("S_Ostan");
            this.Property(t => t.S_Shahrestan).HasColumnName("S_Shahrestan");
            this.Property(t => t.S_Bakhsh).HasColumnName("S_Bakhsh");
            this.Property(t => t.Jensiat).HasColumnName("Jensiat");
            this.Property(t => t.Din).HasColumnName("Din");
            this.Property(t => t.Tahol).HasColumnName("Tahol");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.Mobile).HasColumnName("Mobile");
            this.Property(t => t.Tell).HasColumnName("Tell");
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.NezamVazife).HasColumnName("NezamVazife");
            this.Property(t => t.Razmandeh).HasColumnName("Razmandeh");
            this.Property(t => t.ModatJebhe).HasColumnName("ModatJebhe");
            this.Property(t => t.Azadeh).HasColumnName("Azadeh");
            this.Property(t => t.ModatJebheGh).HasColumnName("ModatJebheGh");
            this.Property(t => t.Janbaz).HasColumnName("Janbaz");
            this.Property(t => t.MizanJanbazi).HasColumnName("MizanJanbazi");
            this.Property(t => t.ModatEsarat).HasColumnName("ModatEsarat");
            this.Property(t => t.Shahid).HasColumnName("Shahid");
            this.Property(t => t.TedadShahid).HasColumnName("TedadShahid");
            this.Property(t => t.BasijFal).HasColumnName("BasijFal");
            this.Property(t => t.NesbatShahid).HasColumnName("NesbatShahid");
            // Relationships
            this.HasRequired(t => t.User).
                WithOptional(t=> t.P_Info);

        }
    }
}
