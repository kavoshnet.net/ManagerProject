using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class Other_SkillMap : EntityTypeConfiguration<Other_Skill>
    {
        public Other_SkillMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            //this.Property(t => t.Lname)
            //    .IsRequired();




            // Table & Column Mappings
            this.ToTable("Other_Skills");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Onvan_Skill).HasColumnName("Onvan_Skill");
            this.Property(t => t.Sharh_Skill).HasColumnName("Sharh_Skill");
            this.Property(t => t.UserID).HasColumnName("UserID");
            // Relationships
            this.HasOptional(t => t.User)
                .WithMany(t => t.Other_Skills)
                .HasForeignKey(d => d.UserID);

        }
    }
}
