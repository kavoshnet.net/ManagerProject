using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class Member_MootMap : EntityTypeConfiguration<Member_Moot>
    {
        public Member_MootMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            //this.Property(t => t.Lname)
            //    .IsRequired();




            // Table & Column Mappings
            this.ToTable("Member_Moots");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Date_Shoro).HasColumnName("Date_Shoro");
            this.Property(t => t.Onvan_Dastgah).HasColumnName("Onvan_Dastgah");
            this.Property(t => t.NoHamkari_Moot).HasColumnName("NoHamkari_Moot");
            this.Property(t => t.Date_Shoro).HasColumnName("Date_Shoro");
            this.Property(t => t.Date_khateme).HasColumnName("Date_khateme");
            this.Property(t => t.UserID).HasColumnName("UserID");
            // Relationships
            this.HasOptional(t => t.User)
                .WithMany(t => t.Member_Moots)
                .HasForeignKey(d => d.UserID);

        }
    }
}
