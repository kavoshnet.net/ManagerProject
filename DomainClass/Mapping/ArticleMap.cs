using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class ArticleMap : EntityTypeConfiguration<Article>
    {
        public ArticleMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            //this.Property(t => t.Lname)
            //    .IsRequired();




            // Table & Column Mappings
            this.ToTable("Articles");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Onvan_Article).HasColumnName("Onvan_Article");
            this.Property(t => t.NoMaghale).HasColumnName("NoMaghale");
            this.Property(t => t.Nasher).HasColumnName("Nasher");
            this.Property(t => t.SaleNashr).HasColumnName("SaleNashr");
            this.Property(t => t.UserID).HasColumnName("UserID");
            // Relationships
            this.HasOptional(t => t.User)
                .WithMany(t => t.Articles)
                .HasForeignKey(d => d.UserID);

        }
    }
}
