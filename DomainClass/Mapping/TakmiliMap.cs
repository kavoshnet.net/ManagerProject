using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class TakmiliMap : EntityTypeConfiguration<Takmili>
    {
        public TakmiliMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            //this.Property(t => t.Lname)
            //    .IsRequired();




            // Table & Column Mappings
            this.ToTable("Takmilis");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.TahsilatHozavi).HasColumnName("TahsilatHozavi");
            this.Property(t => t.Fani).HasColumnName("Fani");
            this.Property(t => t.Ejtemae).HasColumnName("Ejtemae");
            this.Property(t => t.Farhangi).HasColumnName("Farhangi");
            this.Property(t => t.Edari).HasColumnName("Edari");
            this.Property(t => t.Fanavari).HasColumnName("Fanavari");
            this.Property(t => t.Sayer).HasColumnName("Sayer");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Siasat).HasColumnName("Siasat");
            this.Property(t => t.Eghdamat).HasColumnName("Eghdamat");
            this.Property(t => t.Dekhalat).HasColumnName("Dekhalat");
            this.Property(t => t.HamkariDakheli).HasColumnName("HamkariDakheli");
            this.Property(t => t.HamkariKhareji).HasColumnName("HamkariKhareji");
            this.Property(t => t.HamkariMokhtalef).HasColumnName("HamkariMokhtalef");
            this.Property(t => t.DaneshElmi).HasColumnName("DaneshElmi");
            this.Property(t => t.IjadeKhalaghiat).HasColumnName("IjadeKhalaghiat");
            this.Property(t => t.SabkeModiriat).HasColumnName("SabkeModiriat");
            this.Property(t => t.GH_Modiriat).HasColumnName("GH_Modiriat");
            this.Property(t => t.GH_Mohasebat).HasColumnName("GH_Mohasebat");
            this.Property(t => t.GH_Elhagi).HasColumnName("GH_Elhagi");
            this.Property(t => t.GH_Takhalofat).HasColumnName("GH_Takhalofat");
            this.Property(t => t.GH_Sayer).HasColumnName("GH_Sayer");
            this.Property(t => t.GH_SheShom).HasColumnName("GH_SheShom");
            this.Property(t => t.Tozihat_Zarori).HasColumnName("Tozihat_Zarori");
            this.Property(t => t.Date_Takmil).HasColumnName("Date_Takmil");
            this.Property(t => t.LFName_Masol).HasColumnName("LFName_Masol");
            // Relationships
            this.HasRequired(t => t.User)
                .WithOptional(t => t.Takmili);

        }
    }
}
