using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class Job_GovMap : EntityTypeConfiguration<Job_Gov>
    {
        public Job_GovMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            //this.Property(t => t.Lname)
            //    .IsRequired();




            // Table & Column Mappings
            this.ToTable("Job_Goves");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Onvan_Job).HasColumnName("Onvan_Job");
            this.Property(t => t.Dastgah).HasColumnName("Dastgah");
            this.Property(t => t.Vahed).HasColumnName("Vahed");
            this.Property(t => t.DateEntesabFrom).HasColumnName("DateEntesabFrom");
            this.Property(t => t.DateEntesabTo).HasColumnName("DateEntesabTo");
            this.Property(t => t.UserID).HasColumnName("UserID");
            // Relationships
            this.HasOptional(t => t.User)
                .WithMany(t => t.Job_Govs)
                .HasForeignKey(d => d.UserID);

        }
    }
}
