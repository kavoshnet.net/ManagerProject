using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class DegreeMap : EntityTypeConfiguration<Degree>
    {
        public DegreeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            //this.Property(t => t.Lname)
            //    .IsRequired();




            // Table & Column Mappings
            this.ToTable("Degrees");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Onvan_Deg).HasColumnName("Onvan_Deg");
            this.Property(t => t.NoTaghdir).HasColumnName("NoTaghdir");
            this.Property(t => t.Magham_Deg).HasColumnName("Magham_Deg");
            this.Property(t => t.Date_Deg).HasColumnName("Date_Deg");
            this.Property(t => t.Elat_Deg).HasColumnName("Elat_Deg");
            this.Property(t => t.UserID).HasColumnName("UserID");
            // Relationships
            this.HasOptional(t => t.User)
                .WithMany(t => t.Degrees)
                .HasForeignKey(d => d.UserID);

        }
    }
}
