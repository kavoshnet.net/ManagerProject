﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MyClasses;
namespace DomainClass
{
    public class Job_Train
    {
        public Job_Train()
        {

        }
        [Key]
        public Int64 ID { get; set; }

        [Display(Name = "عنوان دوره آموزشی گذرانده")]
        [StringLength(250)]
        public string Onvan_Train { get; set; }

        [Display(Name = "دستگاه برگزار کننده")]
        [StringLength(250)]
        public string Dastgah_Train { get; set; }

        [Display(Name = "تاریخ برگزاری")]
        [StringLength(10)]
        public string Date_Train { get; set; }

        [Display(Name = "مدت زمان دوره")]
        public int? Modat_Train { get; set; }

        [Display(Name = "محل برگزاری دوره")]
        [StringLength(250)]
        public string Mahal_Train { get; set; }


        public Int64? UserID { get; set; }
        public virtual User User { get; set; }
    }
}
