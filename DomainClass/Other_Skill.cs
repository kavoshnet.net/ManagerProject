﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MyClasses;
namespace DomainClass
{
    public class Other_Skill
    {
        public Other_Skill()
        {

        }
        [Key]
        public Int64 ID { get; set; }

        [Display(Name = "عنوان")]
        [StringLength(250)]
        public string Onvan_Skill { get; set; }

        [Display(Name = "شرح")]
        [StringLength(250)]
        public string Sharh_Skill { get; set; }

        public Int64? UserID { get; set; }
        public virtual User User { get; set; }
    }
}
