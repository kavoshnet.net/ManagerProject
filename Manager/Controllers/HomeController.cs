﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Net;
using DataLayer;
using DomainClass;
using System.Web.Security;
using DomainClass.ViewModel;
using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;
using MyClasses;
namespace Manager.Controllers
{
    public partial class HomeController : Controller
    {
        private DataBaseContext db = new DataBaseContext();
        private DataBaseContext Mydb = new DataBaseContext();
        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult LogIn(string returnUrl)
        {
            if (User.Identity.IsAuthenticated) //remember me
            {
                if (ShouldRedirect(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                return Redirect(FormsAuthentication.DefaultUrl);
            }

            return View(); // show the login page
        }

        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult LogIn(Account loginInfo, string returnUrl)
        {
            if (this.ModelState.IsValid)
            {
                using (var usersContext = new DataBaseContext())
                {
                    var user = usersContext.Users.SingleOrDefault(u => u.UserName == loginInfo.UserName && u.Password == loginInfo.Password);
                    if (user != null)
                    {
                        FormsAuthentication.SetAuthCookie(loginInfo.UserName, loginInfo.RememberMe);
                        if (ShouldRedirect(returnUrl))
                        {
                            return Redirect(returnUrl);
                        }
                        FormsAuthentication.RedirectFromLoginPage(loginInfo.UserName, loginInfo.RememberMe);
                    }
                }
            }
            this.ModelState.AddModelError("", "نام کاربری یا کلمه عبور اشتباه است");
            ViewBag.Error = "ورود شما مجاز نیست لطفا کلمه عبور و نام کاربری را به طور صحیح وارد کنید!";
            return View(loginInfo);
        }
        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon(); // it will clear the session at the end of request
            return RedirectToAction(MVC.Home.LogIn());
        }
        private bool ShouldRedirect(string returnUrl)
        {
            // it's a security check
            return !string.IsNullOrWhiteSpace(returnUrl) &&
                                Url.IsLocalUrl(returnUrl) &&
                                returnUrl.Length > 1 &&
                                returnUrl.StartsWith("/", StringComparison.Ordinal) &&
                                !returnUrl.StartsWith("//", StringComparison.Ordinal) &&
                                !returnUrl.StartsWith("/\\", StringComparison.Ordinal);
        }

        public virtual ActionResult Index()
        {

            //یادداشت نحوه ورود و انتقال در اینجا باید مشخصص گردد
            if (User.IsInRole("Admin"))
            {
                return RedirectToAction(MVC.Admin.Index());
            }
            if (User.IsInRole("User"))
            {
                return RedirectToAction(MVC.P_Info.Index());
            }
            //if (User.IsInRole("Nothing"))
            //{
            //    //return RedirectToAction(MVC.Activity.Index());
            //}
            if (!Request.IsAuthenticated)
            {
                //return RedirectToAction(MVC.Home.AccessDenied());
                return View();
            }
            //return RedirectToAction(MVC.P_Info.Index());
            return View();
        }
        [AllowAnonymous]
        public virtual ActionResult AccessDenied()
        {
            return View();
        }

        public virtual ActionResult Register()
        {
            //ViewBag.RoleID = new SelectList(db.Roles, "ID", "RoleName");
            return View();
        }

        // POST: Register/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Register([Bind(Include = "ID,Lname,Fname,UserName,Password")] User user, HttpPostedFileBase UserPic)
        {
            if (ModelState.IsValid)
            {
                if (!MyClasses.Util.IsValidShMeli(user.UserName))
                {
                    this.ModelState.AddModelError("", "در ورد شماره ملی دقت کنید");
                    //TempData["msg"] = "در ورد شماره ملی دقت کنید!";
                }
                else if (db.Users.Where(u => u.UserName == user.UserName).FirstOrDefault() != null)
                {
                    this.ModelState.AddModelError("", "با این شماره ملی قبلاً ثبت نام شده است");
                }
                else
                {
                    if (UserPic != null)
                    {
                        byte[] uploadFile = new byte[UserPic.InputStream.Length];
                        UserPic.InputStream.Read(uploadFile, 0, uploadFile.Length);
                        user.UserPic = uploadFile;
                    }
                    user.RoleID = 2;
                    db.Users.Add(user);
                    db.SaveChanges();
                    return RedirectToAction("login");

                }
            }

            //ViewBag.RoleID = new SelectList(db.Roles, "ID", "RoleName", user.RoleID);
            return View(user);
        }
        // GET: Edu_Rec/Edit/5
        public virtual ActionResult UserProfile(long? id)
        {
            //var id = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", edu_Rec.UserID);

            return View(user);
        }

        // POST: Edu_Rec/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult UserProfile([Bind(Include = "ID,Lname,Fname,UserName,Password")] User user, HttpPostedFileBase UserPic)
        {
            if (ModelState.IsValid)
            {
                //var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                //user = db.Users.Find(ID);
                if (UserPic != null)
                {
                    byte[] uploadFile = new byte[UserPic.InputStream.Length];
                    UserPic.InputStream.Read(uploadFile, 0, uploadFile.Length);
                    user.UserPic = uploadFile;
                }
                else
                {
                    var base64 = Mydb.Users.FirstOrDefault(u => u.ID == user.ID).UserPic;
                    user.UserPic = base64;
                }
                db.Entry(user).State = EntityState.Modified;
                user.RoleID = Mydb.Users.FirstOrDefault(u => u.ID == user.ID).RoleID;
                db.SaveChanges();
                return RedirectToAction("index");
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", edu_Rec.UserID);
            return View(user);

        }

        public virtual ActionResult Report(long? id)
        {
            //------------------------------------------------------------------------------
            //StiReport report = new StiReport();
            //var UserList = db.Users.ToList();
            //string Path = Server.MapPath("~/Report/UserReport.mrt");  // نام و مسیر قالبی که در دیزانر ایجاد کردیم
            //report.Load(Path);
            //report.RegBusinessObject("UserObject", UserList); // نام شیئی که در دیزانر ایجاد کردیم
            //report.Dictionary.SynchronizeBusinessObjects(2);
            //return StiMvcViewer.GetReportSnapshotResult(HttpContext, report);
            //------------------------------------------------------------------------------
            // ایجاد شی جدید
            //var mainReport = new StiReport();
            //// فراخوانی فایل استیمول
            //mainReport.Load(Server.MapPath("~/Report/UserReport.mrt"));
            //var UserID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            //mainReport["UserID"] = UserID;
            //mainReport.Compile();
            //// معرفی نام business object تعریف شده در فایل استیمول + ارسال مدل
            //return Stimulsoft.Report.Mvc.StiMvcViewer.GetReportSnapshotResult(mainReport);

            var MainReport = new StiReport();
            // فراخوانی فایل استیمول
            var UserID = id;//Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            //------------------------------------------------------------------
            var ArticlesList = db.Articles.Where(a => a.UserID == UserID).AsEnumerable().Select(a => new
            {
                a.ID,
                a.Onvan_Article,
                NoMaghale = a.NoMaghale.GetDisplayName(),
                a.Nasher,
                a.SaleNashr,
                a.UserID
            }).ToList();

            var BooksList = db.Books.Where(t => t.UserID == UserID).AsEnumerable().Select(b => new
            {
                b.ID,
                b.Onvan_Book,
                NoAsar = b.NoAsar.GetDisplayName(),
                b.Nasher,
                b.SaleNashr,
                b.UserID
            }).ToList();

            var CheersList = db.Cheers.Where(c => c.UserID == UserID).AsEnumerable().Select(c => new
            {
                c.ID,
                c.Dastgah_Cheer,
                c.Magham_Cheer,
                SathTashvigh = c.SathTashvigh.GetDisplayName(),
                c.Date_Cheer,
                c.Elat_Cheer,
                c.UserID
            }).ToList();

            var DegreesList = db.Degrees.Where(d => d.UserID == UserID).AsEnumerable().Select(d => new
            {
                d.ID,
                d.Onvan_Deg,
                NoTaghdir = d.NoTaghdir.GetDisplayName(),
                d.Magham_Deg,
                d.Date_Deg,
                d.Elat_Deg,
                d.UserID
            }).ToList();

            var E_StatusesList = db.E_Statuses.Where(e => e.ID == UserID).AsEnumerable().Select(e => new
            {
                e.ID,
                e.Shp,
                NoEstekhdam = e.NoEstekhdam.GetDisplayName(),
                NoMoghararatEstekhdam = e.NoMoghararatEstekhdam.GetDisplayName(),
                VaziatKhedmat = e.VaziatKhedmat.GetDisplayName(),
                e.DastgahMahalKhedmat,
                e.VahedSazmani,
                e.G_Ostan,
                e.G_Shahrestan,
                e.G_Bakhsh,
                e.DastgahMamor,
                e.DateMamoriat,
                e.DateShoroKhedmat,
                e.DateKhedmatGH,
                e.ShPost,
                e.OnvanRaste,
                e.OnvanReshte,
                e.TabagheShoghli,
                RotbeShoghli = e.RotbeShoghli.GetDisplayName(),
                e.OnvanShoghl,
                e.FromDate,
                e.Paye,
                e.Martabe,
                e.Nomre1Ghabl,
                e.Nomre2Ghabl,
                e.Nomre3Ghabl,
                e.T_Sal,
                e.T_Mah,
                e.DateEntesab,
                e.K_Sal,
                e.K_Mah,
                e.G_Sal,
                e.G_Mah,
                SathModiriat = e.SathModiriat.GetDisplayName(),
            }).ToList();

            var Edu_RecsList = db.Edu_Recs.Where(e => e.UserID == UserID).AsEnumerable().Select(e => new
            {
                e.ID,
                Maghta = e.Maghta.GetDisplayName(),
                e.Reshte,
                e.Date_Akhz,
                MahalAkhz = e.MahalAkhz.GetDisplayName(),
                e.OnvanDaneshgah,
                e.Keshvar,
                e.Ostan,
                e.Shahrestan,
                e.Shahr,
                e.UserID
            }).ToList();

            var InServ_TrainsList = db.InServ_Trains.Where(i => i.UserID == UserID).AsEnumerable().Select(i => new
            {
                i.ID,
                i.Modat_Train,
                i.Mahal_Train,
                i.UserID,
                i.Onvan_Train,
                i.Dastgah_Train,
                i.Date_Train
            }).ToList();

            var Job_GovsList = db.Job_Govs.Where(j => j.UserID == UserID).AsEnumerable().Select(j => new
            {
                j.ID,
                j.Onvan_Job,
                j.Dastgah,
                j.Vahed,
                j.DateEntesabFrom,
                j.DateEntesabTo,
                j.UserID
            }).ToList();

            var Job_NoGovsList = db.Job_NoGovs.Where(j => j.UserID == UserID).AsEnumerable().Select(j => new
            {
                j.ID,
                j.Onvan_Job,
                j.Dastgah,
                j.Vahed,
                j.DateShoro,
                j.DatePayan,
                j.UserID
            }).ToList();

            var Job_PlansList = db.Job_Plans.Where(j => j.UserID == UserID).AsEnumerable().Select(j => new
            {
                j.ID,
                j.Onvan_Plan,
                NoHamkari = j.NoHamkari.GetDisplayName(),
                VaziatTarh = j.VaziatTarh.GetDisplayName(),
                j.DateEjra,
                j.Natije,
                j.UserID,
            }).ToList();

            var Job_TrainsList = db.Job_Trains.Where(j => j.UserID == UserID).AsEnumerable().Select(j => new
            {
                j.ID,
                j.Onvan_Train,
                j.Dastgah_Train,
                j.Date_Train,
                j.Modat_Train,
                j.Mahal_Train,
                j.UserID,
            }).ToList();

            var Member_MootsList = db.Member_Moots.Where(m => m.UserID == UserID).AsEnumerable().Select(m => new
            {
                m.ID,
                m.Onvan_Shora,
                m.Onvan_Dastgah,
                NoHamkari_Moot = m.NoHamkari_Moot.GetDisplayName(),
                m.Date_Shoro,
                m.Date_khateme,
                m.UserID,
            }).ToList();

            var Other_SkillsList = db.Other_Skills.Where(o => o.UserID == UserID).AsEnumerable().Select(o => new
            {
                o.ID,
                o.Onvan_Skill,
                o.Sharh_Skill,
                o.UserID,
            }).ToList();

            var P_InfosList = db.P_Infos.Where(p => p.ID == UserID).AsEnumerable().Select(p => new
            {
                p.ID,
                p.Lname,
                p.Fname,
                p.FaName,
                p.BDate,
                p.Din,
                p.Shahid,
                p.Razmandeh,
                p.Azadeh,
                p.Janbaz,
                p.MizanJanbazi,
                p.ModatEsarat,
                p.ModatJebhe,
                p.TedadShahid,
                p.BasijFal,
                NesbatShahid = p.NesbatShahid.GetDisplayName(),
                Tahol = p.Tahol.GetDisplayName(),
                p.Email,
                p.Fax,
                p.Mobile,
                p.Tell,
                p.Code,
                NezamVazife = p.NezamVazife.GetDisplayName(),
                p.T_Ostan,
                p.T_Shahrestan,
                p.T_Bakhsh,
                p.ShSh,
                p.S_Ostan,
                p.S_Shahrestan,
                p.S_Bakhsh,
                Jensiat = p.Jensiat.GetDisplayName(),
            }).ToList();

            var Pub_TrainsList = db.Pub_Trains.Where(p => p.UserID == UserID).AsEnumerable().Select(p => new
            {
                p.ID,
                p.Onvan_Train,
                p.Dastgah_Train,
                p.Date_Train,
                p.Modat_Train,
                p.Mahal_Train,
                p.UserID,
            }).ToList();

            var Res_HistorysList = db.Res_Historys.Where(r => r.UserID == UserID).AsEnumerable().Select(r => new
            {
                r.ID,
                r.Onvan_Res,
                NoPajohesh = r.NoPajohesh.GetDisplayName(),
                r.Dastgah,
                r.Magham,
                NoHamkariPajohesh = r.NoHamkariPajohesh.GetDisplayName(),
                r.DateTasvib,
                r.Modat,
                r.UserID,
            }).ToList();

            var RolesList = db.Roles.Where(r => r.ID == UserID).AsEnumerable().Select(r => new
            {
                r.ID,
                r.RoleName,
                r.RoleCaption,
            }).ToList();

            var TakmilisList = db.Takmilis.Where(t => t.ID == UserID).AsEnumerable().Select(t => new
            {
                t.ID,
                t.Fani,
                t.Ejtemae,
                t.Farhangi,
                t.Edari,
                t.Fanavari,
                t.Sayer,
                t.DaneshElmi,
                t.IjadeKhalaghiat,
                t.SabkeModiriat,
                t.GH_Modiriat,
                t.GH_Mohasebat,
                t.GH_Elhagi,
                t.GH_Takhalofat,
                t.GH_Sayer,
                t.GH_SheShom,
                t.Tozihat_Zarori,
                t.Date_Takmil,
                t.LFName_Masol,
                t.Description,
                t.Siasat,
                t.Eghdamat,
                t.Dekhalat,
                t.HamkariDakheli,
                t.HamkariKhareji,
                t.HamkariMokhtalef,
                TahsilatHozavi = t.TahsilatHozavi.GetDisplayName(),
            }).ToList();

            var UsersList = db.Users.Where(u => u.ID == UserID).AsEnumerable().Select(u => new
            {
                u.ID,
                u.Lname,
                u.Fname,
                u.UserName,
                u.Password,
                u.RoleID,
                u.UserPic,
            }).ToList();
            //------------------------------------------------------------------


            //******************************************************************
            MainReport.RegBusinessObject("Articles", ArticlesList );
            MainReport.RegBusinessObject("Books", BooksList);
            MainReport.RegBusinessObject("Cheers", CheersList );
            MainReport.RegBusinessObject("Degrees", DegreesList);
            MainReport.RegBusinessObject("E_Statuses", E_StatusesList);
            MainReport.RegBusinessObject("Edu_Recs", Edu_RecsList);
            MainReport.RegBusinessObject("InServ_Trains", InServ_TrainsList);
            MainReport.RegBusinessObject("Job_Goves", Job_GovsList);
            MainReport.RegBusinessObject("Job_NoGoves", Job_NoGovsList);
            MainReport.RegBusinessObject("Job_Plans", Job_PlansList );
            MainReport.RegBusinessObject("Job_Trains", Job_TrainsList );
            MainReport.RegBusinessObject("Member_Moots", Member_MootsList);
            MainReport.RegBusinessObject("Other_Skills", Other_SkillsList);
            MainReport.RegBusinessObject("P_Infos", P_InfosList);
            MainReport.RegBusinessObject("Pub_Trains", Pub_TrainsList);
            MainReport.RegBusinessObject("Res_Historys", Res_HistorysList);
            MainReport.RegBusinessObject("Roles", RolesList);
            MainReport.RegBusinessObject("Takmilis", TakmilisList);
            MainReport.RegBusinessObject("Users", UsersList);
            //******************************************************************


            //MainReport["UserID"] = UserID;
            MainReport.Load(Server.MapPath("~/Report/UserReport.mrt"));
            MainReport.Compile();
            // معرفی نام business object تعریف شده در فایل استیمول + ارسال مدل
            return StiMvcViewer.GetReportSnapshotResult(MainReport);
        }

        //ایجاد پرینت
        public virtual ActionResult PrintReport()
        {
            return StiMvcViewer.PrintReportResult(this.HttpContext);
        }
        //ایجاد خروجی
        public virtual ActionResult ExportReport()
        {
            return StiMvcViewer.ExportReportResult(this.HttpContext);
        }

        public virtual ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

    }
}