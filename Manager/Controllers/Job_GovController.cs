﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using Manager.Helpers;

namespace Manager.Controllers
{
    [CustomAuthorize(Roles = "User")]
    public partial class Job_GovController : Controller
    {
        private DataBaseContext db = new DataBaseContext();

        // GET: Job_Gov
        public virtual ActionResult Index()
        {
            var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            var job_Govs = db.Job_Govs.Include(j => j.User).Where(j => j.UserID == ID);
            return View(job_Govs.ToList());
        }

        // GET: Job_Gov/Details/5
        public virtual ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Gov job_Gov = db.Job_Govs.Find(id);
            if (job_Gov == null)
            {
                return HttpNotFound();
            }
            return View(job_Gov);
        }

        // GET: Job_Gov/Create
        public virtual ActionResult Create()
        {
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname");
            return View();
        }

        // POST: Job_Gov/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "ID,Onvan_Job,Dastgah,Vahed,DateEntesabFrom,DateEntesabTo,UserID")] Job_Gov job_Gov)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                job_Gov.UserID = ID;
                db.Job_Govs.Add(job_Gov);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", job_Gov.UserID);
            return View(job_Gov);
        }

        // GET: Job_Gov/Edit/5
        public virtual ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Gov job_Gov = db.Job_Govs.Find(id);
            if (job_Gov == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", job_Gov.UserID);
            return View(job_Gov);
        }

        // POST: Job_Gov/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "ID,Onvan_Job,Dastgah,Vahed,DateEntesabFrom,DateEntesabTo,UserID")] Job_Gov job_Gov)
        {
            if (ModelState.IsValid)
            {
                db.Entry(job_Gov).State = EntityState.Modified;
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                job_Gov.UserID = ID;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", job_Gov.UserID);
            return View(job_Gov);
        }

        // GET: Job_Gov/Delete/5
        public virtual ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Gov job_Gov = db.Job_Govs.Find(id);
            if (job_Gov == null)
            {
                return HttpNotFound();
            }
            return View(job_Gov);
        }

        // POST: Job_Gov/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(long id)
        {
            Job_Gov job_Gov = db.Job_Govs.Find(id);
            db.Job_Govs.Remove(job_Gov);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
