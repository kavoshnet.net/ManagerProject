﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using Manager.Helpers;

namespace Manager.Controllers
{
    [CustomAuthorize(Roles = "User")]
    public partial class Job_NoGovController : Controller
    {
        private DataBaseContext db = new DataBaseContext();

        // GET: Job_NoGov
        public virtual ActionResult Index()
        {
            var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            var job_NoGovs = db.Job_NoGovs.Include(j => j.User).Where(j => j.UserID == ID);
            return View(job_NoGovs.ToList());
        }

        // GET: Job_NoGov/Details/5
        public virtual ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_NoGov job_NoGov = db.Job_NoGovs.Find(id);
            if (job_NoGov == null)
            {
                return HttpNotFound();
            }
            return View(job_NoGov);
        }

        // GET: Job_NoGov/Create
        public virtual ActionResult Create()
        {
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname");
            return View();
        }

        // POST: Job_NoGov/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "ID,Onvan_Job,Dastgah,Vahed,DateShoro,DatePayan,UserID")] Job_NoGov job_NoGov)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                job_NoGov.UserID = ID;
                db.Job_NoGovs.Add(job_NoGov);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", job_NoGov.UserID);
            return View(job_NoGov);
        }

        // GET: Job_NoGov/Edit/5
        public virtual ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_NoGov job_NoGov = db.Job_NoGovs.Find(id);
            if (job_NoGov == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", job_NoGov.UserID);
            return View(job_NoGov);
        }

        // POST: Job_NoGov/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "ID,Onvan_Job,Dastgah,Vahed,DateShoro,DatePayan,UserID")] Job_NoGov job_NoGov)
        {
            if (ModelState.IsValid)
            {
                db.Entry(job_NoGov).State = EntityState.Modified;
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                job_NoGov.UserID = ID;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", job_NoGov.UserID);
            return View(job_NoGov);
        }

        // GET: Job_NoGov/Delete/5
        public virtual ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_NoGov job_NoGov = db.Job_NoGovs.Find(id);
            if (job_NoGov == null)
            {
                return HttpNotFound();
            }
            return View(job_NoGov);
        }

        // POST: Job_NoGov/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(long id)
        {
            Job_NoGov job_NoGov = db.Job_NoGovs.Find(id);
            db.Job_NoGovs.Remove(job_NoGov);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
