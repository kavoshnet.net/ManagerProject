﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using Manager.Helpers;

namespace Manager.Controllers
{
    [CustomAuthorize(Roles = "User")]
    public partial class Pub_TrainController : Controller
    {
        private DataBaseContext db = new DataBaseContext();

        // GET: Pub_Train
        public virtual ActionResult Index()
        {
            var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            var pub_Trains = db.Pub_Trains.Include(p => p.User).Where(p => p.UserID == ID);
            return View(pub_Trains.ToList());
        }

        // GET: Pub_Train/Details/5
        public virtual ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pub_Train pub_Train = db.Pub_Trains.Find(id);
            if (pub_Train == null)
            {
                return HttpNotFound();
            }
            return View(pub_Train);
        }

        // GET: Pub_Train/Create
        public virtual ActionResult Create()
        {
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname");
            return View();
        }

        // POST: Pub_Train/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "ID,Onvan_Train,Dastgah_Train,Date_Train,Modat_Train,Mahal_Train,UserID")] Pub_Train pub_Train)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                pub_Train.UserID = ID;
                db.Pub_Trains.Add(pub_Train);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", pub_Train.UserID);
            return View(pub_Train);
        }

        // GET: Pub_Train/Edit/5
        public virtual ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pub_Train pub_Train = db.Pub_Trains.Find(id);
            if (pub_Train == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", pub_Train.UserID);
            return View(pub_Train);
        }

        // POST: Pub_Train/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "ID,Onvan_Train,Dastgah_Train,Date_Train,Modat_Train,Mahal_Train,UserID")] Pub_Train pub_Train)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pub_Train).State = EntityState.Modified;
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                pub_Train.UserID = ID;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", pub_Train.UserID);
            return View(pub_Train);
        }

        // GET: Pub_Train/Delete/5
        public virtual ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pub_Train pub_Train = db.Pub_Trains.Find(id);
            if (pub_Train == null)
            {
                return HttpNotFound();
            }
            return View(pub_Train);
        }

        // POST: Pub_Train/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(long id)
        {
            Pub_Train pub_Train = db.Pub_Trains.Find(id);
            db.Pub_Trains.Remove(pub_Train);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
