﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using Manager.Helpers;

namespace Manager.Controllers
{
    [CustomAuthorize(Roles = "User")]
    public partial class Job_PlanController : Controller
    {
        private DataBaseContext db = new DataBaseContext();

        // GET: Job_Plan
        public virtual ActionResult Index()
        {
            var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            var job_Plans = db.Job_Plans.Include(j => j.User).Where(j => j.UserID == ID);
            return View(job_Plans.ToList());
        }

        // GET: Job_Plan/Details/5
        public virtual ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Plan job_Plan = db.Job_Plans.Find(id);
            if (job_Plan == null)
            {
                return HttpNotFound();
            }
            return View(job_Plan);
        }

        // GET: Job_Plan/Create
        public virtual ActionResult Create()
        {
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname");
            return View();
        }

        // POST: Job_Plan/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "ID,Onvan_Plan,NoHamkari,VaziatTarh,DateEjra,Natije,UserID")] Job_Plan job_Plan)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                job_Plan.UserID = ID;
                db.Job_Plans.Add(job_Plan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", job_Plan.UserID);
            return View(job_Plan);
        }

        // GET: Job_Plan/Edit/5
        public virtual ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Plan job_Plan = db.Job_Plans.Find(id);
            if (job_Plan == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", job_Plan.UserID);
            return View(job_Plan);
        }

        // POST: Job_Plan/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "ID,Onvan_Plan,NoHamkari,VaziatTarh,DateEjra,Natije,UserID")] Job_Plan job_Plan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(job_Plan).State = EntityState.Modified;
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                job_Plan.UserID = ID;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", job_Plan.UserID);
            return View(job_Plan);
        }

        // GET: Job_Plan/Delete/5
        public virtual ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Plan job_Plan = db.Job_Plans.Find(id);
            if (job_Plan == null)
            {
                return HttpNotFound();
            }
            return View(job_Plan);
        }

        // POST: Job_Plan/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(long id)
        {
            Job_Plan job_Plan = db.Job_Plans.Find(id);
            db.Job_Plans.Remove(job_Plan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
