﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using Manager.Helpers;

namespace Manager.Controllers
{
    [CustomAuthorize(Roles = "User")]
    public partial class Member_MootController : Controller
    {
        private DataBaseContext db = new DataBaseContext();

        // GET: Member_Moot
        public virtual ActionResult Index()
        {
            var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            var member_Moots = db.Member_Moots.Include(m => m.User).Where(m => m.UserID == ID);
            return View(member_Moots.ToList());
        }

        // GET: Member_Moot/Details/5
        public virtual ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member_Moot member_Moot = db.Member_Moots.Find(id);
            if (member_Moot == null)
            {
                return HttpNotFound();
            }
            return View(member_Moot);
        }

        // GET: Member_Moot/Create
        public virtual ActionResult Create()
        {
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname");
            return View();
        }

        // POST: Member_Moot/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "ID,Onvan_Shora,Onvan_Dastgah,NoHamkari_Moot,Date_Shoro,Date_khateme,UserID")] Member_Moot member_Moot)
        {
            if (ModelState.IsValid)
            {
                db.Member_Moots.Add(member_Moot);
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                member_Moot.UserID = ID;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", member_Moot.UserID);
            return View(member_Moot);
        }

        // GET: Member_Moot/Edit/5
        public virtual ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member_Moot member_Moot = db.Member_Moots.Find(id);
            if (member_Moot == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", member_Moot.UserID);
            return View(member_Moot);
        }

        // POST: Member_Moot/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "ID,Onvan_Shora,Onvan_Dastgah,NoHamkari_Moot,Date_Shoro,Date_khateme,UserID")] Member_Moot member_Moot)
        {
            if (ModelState.IsValid)
            {
                db.Entry(member_Moot).State = EntityState.Modified;
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                member_Moot.UserID = ID;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", member_Moot.UserID);
            return View(member_Moot);
        }

        // GET: Member_Moot/Delete/5
        public virtual ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member_Moot member_Moot = db.Member_Moots.Find(id);
            if (member_Moot == null)
            {
                return HttpNotFound();
            }
            return View(member_Moot);
        }

        // POST: Member_Moot/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(long id)
        {
            Member_Moot member_Moot = db.Member_Moots.Find(id);
            db.Member_Moots.Remove(member_Moot);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
