﻿using System.Data.Entity;
using DataLayer;
using Manager.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainClass;
using System.Net;
using DomainClass.ViewModel;
using MyClasses;
using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;
using System.Data;
using System.ComponentModel;

namespace Manager.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public partial class AdminController : Controller
    {
        private DataBaseContext db = new DataBaseContext();
        private DataBaseContext Mydb = new DataBaseContext();

        // GET: Admin
        public virtual ActionResult Index()
        {
            //var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            var p_Infos = db.Users;
            return View(p_Infos.ToList());
        }

        public virtual ActionResult UserProfile(long? id)
        {
            //var id = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", edu_Rec.UserID);

            return View(user);
        }

        // POST: Edu_Rec/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult UserProfile([Bind(Include = "ID,Lname,Fname,UserName,Password")] User user, HttpPostedFileBase UserPic)
        {
            if (ModelState.IsValid)
            {
                //var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                //user = db.Users.Find(ID);
                if (UserPic != null)
                {
                    byte[] uploadFile = new byte[UserPic.InputStream.Length];
                    UserPic.InputStream.Read(uploadFile, 0, uploadFile.Length);
                    user.UserPic = uploadFile;
                }
                else
                {
                    var base64 = Mydb.Users.FirstOrDefault(u => u.ID == user.ID).UserPic;
                    user.UserPic = base64;
                }
                db.Entry(user).State = EntityState.Modified;
                user.RoleID = Mydb.Users.FirstOrDefault(u => u.ID == user.ID).RoleID;
                db.SaveChanges();
                return RedirectToAction("index");
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", edu_Rec.UserID);
            return View(user);

        }
        public virtual ActionResult Report()
        {
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Report([Bind(Include = "ID,Lname,Fname,BDate,ShMeli,Jensiat,Maghta,Razmandeh,ModatJebhe," +
            "Azadeh,ModatJebheGh,Janbaz,MizanJanbazi,ModatEsarat,Shahid,TedadShahid,BasijFal,NesbatShahid,ShPost," +
            "OnvanRaste,OnvanReshte,K_Sal,K_Mah,NoEstekhdam,Job_Gov,Nomre1Ghabl,Nomre2Ghabl,Nomre3Ghabl")] BaseReportModel report_model)
        {
            IEnumerable<OutReportModel> search_data = db.Users.
                Where(x => x.Role.RoleName != "Admin").
                Where(x => x.P_Info.ID == x.ID).
                Where(x => x.Edu_Recs.Where(t => t.UserID == x.ID).Count() > 0).
                Where(x=>x.E_Status.ID == x.ID).
                Where(x=>x.Job_Govs.Where(t=>t.UserID == x.ID).Count() > 0).
                //Include(p => p.P_Info).Include(e => e.Edu_Recs).Include(s => s.E_Status).Include(j => j.Job_Govs).
                AsEnumerable().
            Select(x => new OutReportModel
            {
                /*-------------------- P_Info -------------------------------*/
                ID = x.P_Info.User.ID,
                Lname = x.P_Info.Lname,
                Fname = x.P_Info.Fname,
                BDate = x.P_Info.BDate,
                ShMeli = x.P_Info.ShMeli,
                Jensiat = x.P_Info.Jensiat.GetDisplayName(),
                Razmandeh = x.P_Info.Razmandeh,
                ModatJebhe = x.P_Info.ModatJebhe,
                Azadeh = x.P_Info.Azadeh,
                ModatJebheGh = x.P_Info.ModatJebheGh,
                Janbaz = x.P_Info.Janbaz,
                MizanJanbazi = x.P_Info.MizanJanbazi,
                ModatEsarat = x.P_Info.ModatEsarat,
                Shahid = x.P_Info.Shahid,
                TedadShahid = x.P_Info.TedadShahid,
                BasijFal = x.P_Info.BasijFal,
                NesbatShahid = x.P_Info.NesbatShahid.GetDisplayName(),
                /*-------------------- Ede_Res -------------------------------*/
                Maghta = x.Edu_Recs.FirstOrDefault().Maghta.GetDisplayName(),
                /*-------------------- E_status ------------------------------*/
                ShPost = x.E_Status.ShPost,
                OnvanRaste = x.E_Status.OnvanRaste,
                OnvanReshte = x.E_Status.OnvanReshte,
                K_Sal = x.E_Status.K_Sal,
                K_Mah = x.E_Status.K_Mah,
                NoEstekhdam = x.E_Status.NoEstekhdam.GetDisplayName(),
                Nomre1Ghabl = x.E_Status.Nomre1Ghabl,
                Nomre2Ghabl = x.E_Status.Nomre2Ghabl,
                Nomre3Ghabl = x.E_Status.Nomre3Ghabl,
                /*-------------------- Job_Gov -------------------------------*/
                Job_Govs = x.Job_Govs.Select(j =>
                new Job_GovResources
                {
                    ID = j.ID,
                    Onvan_Job = j.Onvan_Job,
                    Dastgah = j.Dastgah,
                    Vahed = j.Vahed,
                    DateEntesabFrom = j.DateEntesabFrom
                ,
                    DateEntesabTo = j.DateEntesabTo
                }).ToList()
            });

            if (report_model.Lname != null)
                search_data = search_data.Where(x => x.Lname.Contains(report_model.Lname));
            if (report_model.Fname != null)
                search_data = search_data.Where(x => x.Fname.Contains(report_model.Fname));
            if (report_model.BDate != null)
                search_data = search_data.Where(x => x.BDate.Contains(report_model.BDate));
            if (report_model.ShMeli != null)
                search_data = search_data.Where(x => x.ShMeli.Contains(report_model.ShMeli));
            if (report_model.Jensiat != null)
                search_data = search_data.Where(x => x.Jensiat == report_model.Jensiat.GetDisplayName());

            if (report_model.Razmandeh != null)
                search_data = search_data.Where(x => x.Razmandeh == report_model.Razmandeh);

            search_data = search_data.Where(x => x.ModatJebhe >= report_model.ModatJebhe);

            if (report_model.Azadeh != null)
                search_data = search_data.Where(x => x.Azadeh == report_model.Azadeh);

            search_data = search_data.Where(x => x.ModatJebheGh >= report_model.ModatJebheGh);

            if (report_model.Janbaz != null)
                search_data = search_data.Where(x => x.Janbaz == report_model.Janbaz);

            search_data = search_data.Where(x => x.MizanJanbazi >= report_model.MizanJanbazi);

            search_data = search_data.Where(x => x.ModatEsarat >= report_model.ModatEsarat);

            if (report_model.Shahid != null)
                search_data = search_data.Where(x => x.Shahid == report_model.Shahid);

            search_data = search_data.Where(x => x.TedadShahid >= report_model.TedadShahid);

            search_data = search_data.Where(x => x.BasijFal >= report_model.BasijFal);

            if (report_model.NesbatShahid != null)
                search_data = search_data.Where(x => x.NesbatShahid == report_model.NesbatShahid.GetDisplayName());

            if (report_model.Maghta != null)
                search_data = search_data.Where(x => x.Maghta == report_model.Maghta.GetDisplayName());

            if (report_model.ShPost != null)
                search_data = search_data.Where(x => x.ShPost.Contains(report_model.ShPost));

            if (report_model.OnvanRaste != null)
                search_data = search_data.Where(x => x.OnvanRaste.Contains(report_model.OnvanRaste));

            if (report_model.OnvanReshte != null)
                search_data = search_data.Where(x => x.OnvanReshte.Contains(report_model.OnvanReshte));

            search_data = search_data.Where(x => x.K_Sal >= report_model.K_Sal);

            search_data = search_data.Where(x => x.K_Mah >= report_model.K_Mah);

            if (report_model.NoEstekhdam != null)
                search_data = search_data.Where(x => x.NoEstekhdam == report_model.NoEstekhdam.GetDisplayName());

            if (report_model.Job_Gov != null)
                if (report_model.Job_Gov == true)
                    search_data = search_data.Where(x => x.Job_Govs.Count > 0);
                else
                    search_data = search_data.Where(x => x.Job_Govs.Count == 0);


            search_data = search_data.Where(x => x.Nomre1Ghabl >= report_model.Nomre1Ghabl);

            search_data = search_data.Where(x => x.Nomre2Ghabl >= report_model.Nomre2Ghabl);

            search_data = search_data.Where(x => x.Nomre2Ghabl >= report_model.Nomre2Ghabl);
            try
            {
                var temp = search_data.ToList();
                ViewBag.Data = temp;
            }
            catch (Exception e)
            {
                return RedirectToAction("Report");
                //return  HttpNotFound();
            }
            //var list = search_data.ToList();

            return View();

        }
        public virtual ActionResult AdminReport(/*string list*/int ID)
        {
            //List<OutReportModel> result = System.Web.Helpers.Json.Decode<List<OutReportModel>>(list);
            //if (list != "" && list != null)
            {
                var MainReport = new StiReport();

                IEnumerable<OutReportModel> search_data = db.Users.
                Where(x => x.Role.RoleName != "Admin").
                Where(x => x.P_Info.ID == x.ID).
                Where(x => x.Edu_Recs.Where(t => t.UserID == x.ID).Count() > 0).
                Where(x => x.E_Status.ID == x.ID).
                Where(x => x.Job_Govs.Where(t => t.UserID == x.ID).Count() > 0).
                Include(p => p.P_Info).Include(e => e.Edu_Recs).Include(s => s.E_Status).Include(j => j.Job_Govs).
                Where(x=>x.ID == ID).
                AsEnumerable().
                Select(x => new OutReportModel
                {
                    /*-------------------- P_Info -------------------------------*/
                    ID = x.P_Info.User.ID,
                    Lname = x.P_Info.Lname,
                    Fname = x.P_Info.Fname,
                    BDate = x.P_Info.BDate,
                    ShMeli = x.P_Info.ShMeli,
                    Jensiat = x.P_Info.Jensiat.GetDisplayName(),
                    Razmandeh = x.P_Info.Razmandeh,
                    ModatJebhe = x.P_Info.ModatJebhe,
                    Azadeh = x.P_Info.Azadeh,
                    ModatJebheGh = x.P_Info.ModatJebheGh,
                    Janbaz = x.P_Info.Janbaz,
                    MizanJanbazi = x.P_Info.MizanJanbazi,
                    ModatEsarat = x.P_Info.ModatEsarat,
                    Shahid = x.P_Info.Shahid,
                    TedadShahid = x.P_Info.TedadShahid,
                    BasijFal = x.P_Info.BasijFal,
                    NesbatShahid = x.P_Info.NesbatShahid.GetDisplayName(),
                    /*-------------------- Ede_Res -------------------------------*/
                    Maghta = x.Edu_Recs.FirstOrDefault().Maghta.GetDisplayName(),
                    /*-------------------- E_status ------------------------------*/
                    ShPost = x.E_Status.ShPost,
                    OnvanRaste = x.E_Status.OnvanRaste,
                    OnvanReshte = x.E_Status.OnvanReshte,
                    K_Sal = x.E_Status.K_Sal,
                    K_Mah = x.E_Status.K_Mah,
                    NoEstekhdam = x.E_Status.NoEstekhdam.GetDisplayName(),
                    Nomre1Ghabl = x.E_Status.Nomre1Ghabl,
                    Nomre2Ghabl = x.E_Status.Nomre2Ghabl,
                    Nomre3Ghabl = x.E_Status.Nomre3Ghabl,
                    /*-------------------- Job_Gov -------------------------------*/
                    Job_Govs = x.Job_Govs.Select(j =>
                    new Job_GovResources
                    {
                        ID = j.ID,
                        Onvan_Job = j.Onvan_Job,
                        Dastgah = j.Dastgah,
                        Vahed = j.Vahed,
                        DateEntesabFrom = j.DateEntesabFrom
                    ,
                        DateEntesabTo = j.DateEntesabTo
                    }).ToList()
                });
                //List<long> ids = new List<long>();
                //ids = list.Split(',').Select(long.Parse).ToList();
                try
                {
                    //var temp = search_data.Where(x => ids.Contains(x.ID)).ToList();

                    var temp = search_data.ToList();
                }
                catch (Exception)
                {
                    return RedirectToAction("Report");

                }
                //var datalist = search_data.Where(x => ids.Contains(x.ID)).ToList();
                var datalist = search_data.Where(x => x.ID==ID).ToList();
                MainReport.RegBusinessObject("OutReportModel", datalist);
                //MainReport.RegBusinessObject("Job_GovModel", search_data.Where(x => ids.Contains(x.ID)).Select(x => x.Job_Govs).ToList());
                MainReport.RegBusinessObject("Job_GovModel", search_data.
                    Where(x => x.ID==ID).Select(x => x.Job_Govs).ToList());
                MainReport.Load(Server.MapPath("~/Report/AdminReport.mrt"));
                MainReport.Compile();
                return StiMvcViewer.GetReportSnapshotResult(MainReport);
            }
            //else
            //{
            //    return RedirectToAction("Report");
            //}

        }


    public virtual ActionResult AdminAllReport(string list)
        {
            //List<OutReportModel> result = System.Web.Helpers.Json.Decode<List<OutReportModel>>(list);
            if (list != "" && list != null)
            {
                var MainReport = new StiReport();

                IEnumerable<OutReportModel> search_data = db.Users.
                Where(x => x.Role.RoleName != "Admin").
                Where(x => x.P_Info.ID == x.ID).
                Where(x => x.Edu_Recs.Where(t => t.UserID == x.ID).Count() > 0).
                Where(x => x.E_Status.ID == x.ID).
                Where(x => x.Job_Govs.Where(t => t.UserID == x.ID).Count() > 0).
                Include(p => p.P_Info).Include(e => e.Edu_Recs).Include(s => s.E_Status).Include(j => j.Job_Govs).
                AsEnumerable().
                Select(x => new OutReportModel
                {
                    /*-------------------- P_Info -------------------------------*/
                    ID = x.P_Info.User.ID,
                    UserID = x.ID,
                    Lname = x.P_Info.Lname,
                    Fname = x.P_Info.Fname,
                    BDate = x.P_Info.BDate,
                    ShMeli = x.P_Info.ShMeli,
                    Jensiat = x.P_Info.Jensiat.GetDisplayName(),
                    Razmandeh = x.P_Info.Razmandeh,
                    ModatJebhe = x.P_Info.ModatJebhe,
                    Azadeh = x.P_Info.Azadeh,
                    ModatJebheGh = x.P_Info.ModatJebheGh,
                    Janbaz = x.P_Info.Janbaz,
                    MizanJanbazi = x.P_Info.MizanJanbazi,
                    ModatEsarat = x.P_Info.ModatEsarat,
                    Shahid = x.P_Info.Shahid,
                    TedadShahid = x.P_Info.TedadShahid,
                    BasijFal = x.P_Info.BasijFal,
                    NesbatShahid = x.P_Info.NesbatShahid.GetDisplayName(),
                    /*-------------------- Ede_Res -------------------------------*/
                    Maghta = x.Edu_Recs.FirstOrDefault().Maghta.GetDisplayName(),
                    /*-------------------- E_status ------------------------------*/
                    ShPost = x.E_Status.ShPost,
                    OnvanRaste = x.E_Status.OnvanRaste,
                    OnvanReshte = x.E_Status.OnvanReshte,
                    K_Sal = x.E_Status.K_Sal,
                    K_Mah = x.E_Status.K_Mah,
                    NoEstekhdam = x.E_Status.NoEstekhdam.GetDisplayName(),
                    Nomre1Ghabl = x.E_Status.Nomre1Ghabl,
                    Nomre2Ghabl = x.E_Status.Nomre2Ghabl,
                    Nomre3Ghabl = x.E_Status.Nomre3Ghabl,
                    /*-------------------- Job_Gov -------------------------------*/
                    Job_Govs = x.Job_Govs.Select(j =>
                    new Job_GovResources
                    {
                        ID = j.ID,
                        Onvan_Job = j.Onvan_Job,
                        Dastgah = j.Dastgah,
                        Vahed = j.Vahed,
                        DateEntesabFrom = j.DateEntesabFrom
                    ,
                        DateEntesabTo = j.DateEntesabTo,
                        UserID = x.ID
                    }).ToList()
                });
                List<long> ids = new List<long>();
                ids = list.Split(',').Select(long.Parse).ToList();
                try
                {
                    var temp = search_data.Where(x => ids.Contains(x.ID)).ToList();

                }
                catch (Exception)
                {
                    return RedirectToAction("Report");

                }
                var datalist = search_data.Where(x => ids.Contains(x.ID)).ToList();
                //var job_govlist = search_data.Where(x => ids.Contains(x.ID)).Select(x => x.Job_Govs).ToList();
                MainReport.RegBusinessObject("OutReportModel", datalist);
                //MainReport.RegBusinessObject("Job_GovModel", job_govlist);
                MainReport.Load(Server.MapPath("~/Report/AdminAllReport.mrt"));
                MainReport.Compile();
                return StiMvcViewer.GetReportSnapshotResult(MainReport);
            }
            else
            {
                return RedirectToAction("Report");
            }

        }
        public virtual ActionResult PrintReport()
        {
            return StiMvcViewer.PrintReportResult(this.HttpContext);
        }
        //ایجاد خروجی
        public virtual ActionResult ExportReport()
        {
            return StiMvcViewer.ExportReportResult(this.HttpContext);
        }

        public virtual ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }


    }
}