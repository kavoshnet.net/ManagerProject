﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using Manager.Helpers;

namespace Manager.Controllers
{
    [CustomAuthorize(Roles = "User")]
    public partial class Res_HistoryController : Controller
    {
        private DataBaseContext db = new DataBaseContext();

        // GET: Res_History
        public virtual ActionResult Index()
        {
            var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            var res_Historys = db.Res_Historys.Include(r => r.User).Where(r => r.UserID == ID);
            return View(res_Historys.ToList());
        }

        // GET: Res_History/Details/5
        public virtual ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Res_History res_History = db.Res_Historys.Find(id);
            if (res_History == null)
            {
                return HttpNotFound();
            }
            return View(res_History);
        }

        // GET: Res_History/Create
        public virtual ActionResult Create()
        {
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname");
            return View();
        }

        // POST: Res_History/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "ID,Onvan_Res,NoPajohesh,Dastgah,Magham,NoHamkariPajohesh,DateTasvib,Modat,UserID")] Res_History res_History)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                res_History.UserID = ID;
                db.Res_Historys.Add(res_History);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", res_History.UserID);
            return View(res_History);
        }

        // GET: Res_History/Edit/5
        public virtual ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Res_History res_History = db.Res_Historys.Find(id);
            if (res_History == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", res_History.UserID);
            return View(res_History);
        }

        // POST: Res_History/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "ID,Onvan_Res,NoPajohesh,Dastgah,Magham,NoHamkariPajohesh,DateTasvib,Modat,UserID")] Res_History res_History)
        {
            if (ModelState.IsValid)
            {
                db.Entry(res_History).State = EntityState.Modified;
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                res_History.UserID = ID;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", res_History.UserID);
            return View(res_History);
        }

        // GET: Res_History/Delete/5
        public virtual ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Res_History res_History = db.Res_Historys.Find(id);
            if (res_History == null)
            {
                return HttpNotFound();
            }
            return View(res_History);
        }

        // POST: Res_History/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(long id)
        {
            Res_History res_History = db.Res_Historys.Find(id);
            db.Res_Historys.Remove(res_History);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
