﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using Manager.Helpers;

namespace Manager.Controllers
{
    [CustomAuthorize(Roles = "User")]
    public partial class Edu_RecController : Controller
    {
        private DataBaseContext db = new DataBaseContext();

        // GET: Edu_Rec
        public virtual ActionResult Index()
        {
            var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            var edu_Recs = db.Edu_Recs.Include(e => e.User).Where(e => e.UserID == ID);
            return View(edu_Recs.ToList());
        }

        // GET: Edu_Rec/Details/5
        public virtual ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Edu_Rec edu_Rec = db.Edu_Recs.Find(id);
            if (edu_Rec == null)
            {
                return HttpNotFound();
            }
            return View(edu_Rec);
        }

        // GET: Edu_Rec/Create
        public virtual ActionResult Create()
        {
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname");
            return View();
        }

        // POST: Edu_Rec/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "ID,Maghta,Reshte,Date_Akhz,MahalAkhz,OnvanDaneshgah,Keshvar,Ostan,Shahrestan,Shahr,UserID")] Edu_Rec edu_Rec)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                edu_Rec.UserID = ID;
                db.Edu_Recs.Add(edu_Rec);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", edu_Rec.UserID);
            return View(edu_Rec);
        }

        // GET: Edu_Rec/Edit/5
        public virtual ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Edu_Rec edu_Rec = db.Edu_Recs.Find(id);
            if (edu_Rec == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", edu_Rec.UserID);
            return View(edu_Rec);
        }

        // POST: Edu_Rec/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "ID,Maghta,Reshte,Date_Akhz,MahalAkhz,OnvanDaneshgah,Keshvar,Ostan,Shahrestan,Shahr,UserID")] Edu_Rec edu_Rec)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                edu_Rec.UserID = ID;
                db.Entry(edu_Rec).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", edu_Rec.UserID);
            return View(edu_Rec);
        }

        // GET: Edu_Rec/Delete/5
        public virtual ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Edu_Rec edu_Rec = db.Edu_Recs.Find(id);
            if (edu_Rec == null)
            {
                return HttpNotFound();
            }
            return View(edu_Rec);
        }

        // POST: Edu_Rec/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(long id)
        {
            Edu_Rec edu_Rec = db.Edu_Recs.Find(id);
            db.Edu_Recs.Remove(edu_Rec);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
