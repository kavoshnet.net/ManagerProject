﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using Manager.Helpers;

namespace Manager.Controllers
{
    [CustomAuthorize(Roles = "User")]
    public partial class TakmilisController : Controller
    {
        private DataBaseContext db = new DataBaseContext();

        // GET: Takmilis
        public virtual ActionResult Index()
        {
            var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            var takmilis = db.Takmilis.Include(t => t.User).Where(t => t.ID == ID);
            return View(takmilis.ToList());
        }

        // GET: Takmilis/Details/5
        public virtual ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Takmili takmili = db.Takmilis.Find(id);
            if (takmili == null)
            {
                return HttpNotFound();
            }
            return View(takmili);
        }

        // GET: Takmilis/Create
        public virtual ActionResult Create()
        {
            //ViewBag.ID = new SelectList(db.Users, "ID", "Lname");
            return View();
        }

        // POST: Takmilis/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "TahsilatHozavi,Fani,Ejtemae,Farhangi,Edari,Fanavari,Sayer,Description,Siasat,Eghdamat,Dekhalat,HamkariDakheli,HamkariKhareji,HamkariMokhtalef,DaneshElmi,IjadeKhalaghiat,SabkeModiriat,GH_Modiriat,GH_Mohasebat,GH_Elhagi,GH_Takhalofat,GH_Sayer,GH_SheShom,Tozihat_Zarori,Date_Takmil,LFName_Masol")] Takmili takmili)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                takmili.ID = ID;
                db.Takmilis.Add(takmili);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.ID = new SelectList(db.Users, "ID", "Lname", takmili.ID);
            return View(takmili);
        }

        // GET: Takmilis/Edit/5
        public virtual ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Takmili takmili = db.Takmilis.Find(id);
            if (takmili == null)
            {
                return HttpNotFound();
            }
            //ViewBag.ID = new SelectList(db.Users, "ID", "Lname", takmili.ID);
            return View(takmili);
        }

        // POST: Takmilis/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "ID,TahsilatHozavi,Fani,Ejtemae,Farhangi,Edari,Fanavari,Sayer,Description,Siasat,Eghdamat,Dekhalat,HamkariDakheli,HamkariKhareji,HamkariMokhtalef,DaneshElmi,IjadeKhalaghiat,SabkeModiriat,GH_Modiriat,GH_Mohasebat,GH_Elhagi,GH_Takhalofat,GH_Sayer,GH_SheShom,Tozihat_Zarori,Date_Takmil,LFName_Masol")] Takmili takmili)
        {
            if (ModelState.IsValid)
            {
                db.Entry(takmili).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.ID = new SelectList(db.Users, "ID", "Lname", takmili.ID);
            return View(takmili);
        }

        // GET: Takmilis/Delete/5
        public virtual ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Takmili takmili = db.Takmilis.Find(id);
            if (takmili == null)
            {
                return HttpNotFound();
            }
            return View(takmili);
        }

        // POST: Takmilis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(long id)
        {
            Takmili takmili = db.Takmilis.Find(id);
            db.Takmilis.Remove(takmili);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
