﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using Manager.Helpers;

namespace Manager.Controllers
{
    [CustomAuthorize(Roles = "User")]
    public partial class Job_TrainController : Controller
    {
        private DataBaseContext db = new DataBaseContext();

        // GET: Job_Train
        public virtual ActionResult Index()
        {
            var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            var job_Trains = db.Job_Trains.Include(j => j.User).Where(j => j.UserID == ID);
            return View(job_Trains.ToList());
        }

        // GET: Job_Train/Details/5
        public virtual ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Train job_Train = db.Job_Trains.Find(id);
            if (job_Train == null)
            {
                return HttpNotFound();
            }
            return View(job_Train);
        }

        // GET: Job_Train/Create
        public virtual ActionResult Create()
        {
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname");
            return View();
        }

        // POST: Job_Train/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "ID,Onvan_Train,Dastgah_Train,Date_Train,Modat_Train,Mahal_Train,UserID")] Job_Train job_Train)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                job_Train.UserID = ID;
                db.Job_Trains.Add(job_Train);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", job_Train.UserID);
            return View(job_Train);
        }

        // GET: Job_Train/Edit/5
        public virtual ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Train job_Train = db.Job_Trains.Find(id);
            if (job_Train == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", job_Train.UserID);
            return View(job_Train);
        }

        // POST: Job_Train/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "ID,Onvan_Train,Dastgah_Train,Date_Train,Modat_Train,Mahal_Train,UserID")] Job_Train job_Train)
        {
            if (ModelState.IsValid)
            {
                db.Entry(job_Train).State = EntityState.Modified;
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                job_Train.UserID = ID;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", job_Train.UserID);
            return View(job_Train);
        }

        // GET: Job_Train/Delete/5
        public virtual ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Train job_Train = db.Job_Trains.Find(id);
            if (job_Train == null)
            {
                return HttpNotFound();
            }
            return View(job_Train);
        }

        // POST: Job_Train/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(long id)
        {
            Job_Train job_Train = db.Job_Trains.Find(id);
            db.Job_Trains.Remove(job_Train);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
