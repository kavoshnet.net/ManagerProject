﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using Manager.Helpers;

namespace Manager.Controllers
{
    [CustomAuthorize(Roles = "User")]
    public partial class CheersController : Controller
    {
        private DataBaseContext db = new DataBaseContext();

        // GET: Cheers
        public virtual ActionResult Index()
        {
            var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            var cheers = db.Cheers.Include(c => c.User).Where(c => c.UserID == ID);
            return View(cheers.ToList());
        }

        // GET: Cheers/Details/5
        public virtual ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cheer cheer = db.Cheers.Find(id);
            if (cheer == null)
            {
                return HttpNotFound();
            }
            return View(cheer);
        }

        // GET: Cheers/Create
        public virtual ActionResult Create()
        {
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname");
            return View();
        }

        // POST: Cheers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "ID,Dastgah_Cheer,Magham_Cheer,SathTashvigh,Date_Cheer,Elat_Cheer,UserID")] Cheer cheer)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                cheer.UserID = ID;
                db.Cheers.Add(cheer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", cheer.UserID);
            return View(cheer);
        }

        // GET: Cheers/Edit/5
        public virtual ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cheer cheer = db.Cheers.Find(id);
            if (cheer == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", cheer.UserID);
            return View(cheer);
        }

        // POST: Cheers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "ID,Dastgah_Cheer,Magham_Cheer,SathTashvigh,Date_Cheer,Elat_Cheer,UserID")] Cheer cheer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cheer).State = EntityState.Modified;
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                cheer.UserID = ID;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", cheer.UserID);
            return View(cheer);
        }

        // GET: Cheers/Delete/5
        public virtual ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cheer cheer = db.Cheers.Find(id);
            if (cheer == null)
            {
                return HttpNotFound();
            }
            return View(cheer);
        }

        // POST: Cheers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(long id)
        {
            Cheer cheer = db.Cheers.Find(id);
            db.Cheers.Remove(cheer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
