﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using Manager.Helpers;

namespace Manager.Controllers
{
    [CustomAuthorize(Roles = "User")]
    public partial class P_InfoController : Controller
    {
        private DataBaseContext db = new DataBaseContext();
        // GET: P_Info
        public virtual ActionResult Index()
        {
            var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            var p_Infos = db.P_Infos.Include(p => p.User).Where(p=>p.ID==ID);
            return View(p_Infos.ToList());
        }

        // GET: P_Info/Details/5
        public virtual ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            P_Info p_Info = db.P_Infos.Find(id);
            if (p_Info == null)
            {
                return HttpNotFound();
            }
            return View(p_Info);
        }

        // GET: P_Info/Create
        public virtual ActionResult Create()
        {
            //ViewBag.ID = new SelectList(db.Users, "ID", "Lname");
            return View();
        }

        // POST: P_Info/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "Lname,Fname,FaName,BDate,T_Ostan,T_Shahrestan,T_Bakhsh,ShSh,S_Ostan,S_Shahrestan,S_Bakhsh,Jensiat,Din,Tahol,Email,Fax,Mobile,Tell,Code,NezamVazife,Razmandeh,ModatJebhe,Azadeh,ModatJebheGh,Janbaz,MizanJanbazi,ModatEsarat,Shahid,TedadShahid,BasijFal,NesbatShahid")] P_Info p_Info)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                p_Info.ID = ID;
                p_Info.ShMeli = db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).UserName;
                p_Info.ModatJebhe = p_Info.ModatJebhe == null ? 0 : p_Info.ModatJebhe;
                p_Info.ModatJebheGh = p_Info.ModatJebheGh == null ? 0 : p_Info.ModatJebheGh;
                p_Info.MizanJanbazi = p_Info.MizanJanbazi == null ? 0 : p_Info.MizanJanbazi;
                p_Info.ModatEsarat = p_Info.ModatEsarat == null ? 0 : p_Info.ModatEsarat;
                p_Info.TedadShahid = p_Info.TedadShahid == null ? 0 : p_Info.TedadShahid;
                p_Info.BasijFal = p_Info.BasijFal == null ? 0 : p_Info.BasijFal;


                db.P_Infos.Add(p_Info);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.ID = new SelectList(db.Users, "ID", "Lname", p_Info.ID);
            return View(p_Info);
        }

        // GET: P_Info/Edit/5
        public virtual ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            P_Info p_Info = db.P_Infos.Find(id);
            if (p_Info == null)
            {
                return HttpNotFound();
            }
            //ViewBag.ID = new SelectList(db.Users, "ID", "Lname", p_Info.ID);
            return View(p_Info);
        }

        // POST: P_Info/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "ID,Lname,Fname,FaName,BDate,T_Ostan,T_Shahrestan,T_Bakhsh,ShSh,S_Ostan,S_Shahrestan,S_Bakhsh,Jensiat,Din,Tahol,Email,Fax,Mobile,Tell,Code,NezamVazife,Razmandeh,ModatJebhe,Azadeh,ModatJebheGh,Janbaz,MizanJanbazi,ModatEsarat,Shahid,TedadShahid,BasijFal,NesbatShahid")] P_Info p_Info)
        {
            if (ModelState.IsValid)
            {
                db.Entry(p_Info).State = EntityState.Modified;
                p_Info.ShMeli = db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).UserName;
                p_Info.ModatJebhe = p_Info.ModatJebhe == null ? 0 : p_Info.ModatJebhe;
                p_Info.ModatJebheGh = p_Info.ModatJebheGh == null ? 0 : p_Info.ModatJebheGh;
                p_Info.MizanJanbazi = p_Info.MizanJanbazi == null ? 0 : p_Info.MizanJanbazi;
                p_Info.ModatEsarat = p_Info.ModatEsarat == null ? 0 : p_Info.ModatEsarat;
                p_Info.TedadShahid = p_Info.TedadShahid == null ? 0 : p_Info.TedadShahid;
                p_Info.BasijFal = p_Info.BasijFal == null ? 0 : p_Info.BasijFal;

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.ID = new SelectList(db.Users, "ID", "Lname", p_Info.ID);
            return View(p_Info);
        }

        // GET: P_Info/Delete/5
        public virtual ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            P_Info p_Info = db.P_Infos.Find(id);
            if (p_Info == null)
            {
                return HttpNotFound();
            }
            return View(p_Info);
        }

        // POST: P_Info/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(long id)
        {
            P_Info p_Info = db.P_Infos.Find(id);
            db.P_Infos.Remove(p_Info);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
