﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using Manager.Helpers;

namespace Manager.Controllers
{
    [CustomAuthorize(Roles = "User")]
    public partial class InServ_TrainController : Controller
    {
        private DataBaseContext db = new DataBaseContext();

        // GET: InServ_Train
        public virtual ActionResult Index()
        {
            var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            var inServ_Trains = db.InServ_Trains.Include(i => i.User).Where(i => i.UserID == ID);
            return View(inServ_Trains.ToList());
        }

        // GET: InServ_Train/Details/5
        public virtual ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InServ_Train inServ_Train = db.InServ_Trains.Find(id);
            if (inServ_Train == null)
            {
                return HttpNotFound();
            }
            return View(inServ_Train);
        }

        // GET: InServ_Train/Create
        public virtual ActionResult Create()
        {
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname");
            return View();
        }

        // POST: InServ_Train/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "ID,Onvan_Train,Dastgah_Train,Date_Train,Modat_Train,Mahal_Train,UserID")] InServ_Train inServ_Train)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                inServ_Train.UserID = ID;
                db.InServ_Trains.Add(inServ_Train);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", inServ_Train.UserID);
            return View(inServ_Train);
        }

        // GET: InServ_Train/Edit/5
        public virtual ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InServ_Train inServ_Train = db.InServ_Trains.Find(id);
            if (inServ_Train == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", inServ_Train.UserID);
            return View(inServ_Train);
        }

        // POST: InServ_Train/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "ID,Onvan_Train,Dastgah_Train,Date_Train,Modat_Train,Mahal_Train,UserID")] InServ_Train inServ_Train)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                inServ_Train.UserID = ID;
                db.Entry(inServ_Train).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", inServ_Train.UserID);
            return View(inServ_Train);
        }

        // GET: InServ_Train/Delete/5
        public virtual ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InServ_Train inServ_Train = db.InServ_Trains.Find(id);
            if (inServ_Train == null)
            {
                return HttpNotFound();
            }
            return View(inServ_Train);
        }

        // POST: InServ_Train/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(long id)
        {
            InServ_Train inServ_Train = db.InServ_Trains.Find(id);
            db.InServ_Trains.Remove(inServ_Train);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
