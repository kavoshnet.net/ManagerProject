﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using Manager.Helpers;

namespace Manager.Controllers
{
    [CustomAuthorize(Roles = "User")]
    public partial class E_StatusController : Controller
    {
        private DataBaseContext db = new DataBaseContext();

        // GET: E_Status
        public virtual ActionResult Index()
        {
            var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            var e_Statuses = db.E_Statuses.Include(e => e.User).Where(e => e.ID == ID);
            return View(e_Statuses.ToList());
        }

        // GET: E_Status/Details/5
        public virtual ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            E_Status e_Status = db.E_Statuses.Find(id);
            if (e_Status == null)
            {
                return HttpNotFound();
            }
            return View(e_Status);
        }

        // GET: E_Status/Create
        public virtual ActionResult Create()
        {
            //ViewBag.ID = new SelectList(db.Users, "ID", "Lname");
            return View();
        }

        // POST: E_Status/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "Shp,NoEstekhdam,NoMoghararatEstekhdam,VaziatKhedmat,DastgahMahalKhedmat,VahedSazmani,G_Ostan,G_Shahrestan,G_Bakhsh,DastgahMamor,DateMamoriat,DateShoroKhedmat,DateKhedmatGH,ShPost,OnvanRaste,OnvanReshte,TabagheShoghli,RotbeShoghli,OnvanShoghl,FromDate,Paye,Martabe,Nomre1Ghabl,Nomre2Ghabl,Nomre3Ghabl,T_Sal,T_Mah,DateEntesab,K_Sal,K_Mah,G_Sal,G_Mah,SathModiriat")] E_Status e_Status)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                e_Status.ID = ID;
                e_Status.Nomre1Ghabl = e_Status.Nomre1Ghabl == null ? 0 : e_Status.Nomre1Ghabl;
                e_Status.Nomre2Ghabl = e_Status.Nomre2Ghabl == null ? 0 : e_Status.Nomre2Ghabl;
                e_Status.Nomre3Ghabl = e_Status.Nomre3Ghabl == null ? 0 : e_Status.Nomre3Ghabl;
                e_Status.T_Sal = e_Status.T_Sal == null ? 0 : e_Status.T_Sal;
                e_Status.T_Mah = e_Status.T_Mah == null ? 0 : e_Status.T_Mah;
                e_Status.K_Sal = e_Status.K_Sal == null ? 0 : e_Status.K_Sal;
                e_Status.K_Mah = e_Status.K_Mah == null ? 0 : e_Status.K_Mah;
                e_Status.G_Sal = e_Status.G_Sal == null ? 0 : e_Status.G_Sal;
                e_Status.G_Mah = e_Status.G_Mah == null ? 0 : e_Status.G_Mah;

                db.E_Statuses.Add(e_Status);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.ID = new SelectList(db.Users, "ID", "Lname", e_Status.ID);
            return View(e_Status);
        }

        // GET: E_Status/Edit/5
        public virtual ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            E_Status e_Status = db.E_Statuses.Find(id);
            if (e_Status == null)
            {
                return HttpNotFound();
            }
            //ViewBag.ID = new SelectList(db.Users, "ID", "Lname", e_Status.ID);
            return View(e_Status);
        }

        // POST: E_Status/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "ID,Shp,NoEstekhdam,NoMoghararatEstekhdam,VaziatKhedmat,DastgahMahalKhedmat,VahedSazmani,G_Ostan,G_Shahrestan,G_Bakhsh,DastgahMamor,DateMamoriat,DateShoroKhedmat,DateKhedmatGH,ShPost,OnvanRaste,OnvanReshte,TabagheShoghli,RotbeShoghli,OnvanShoghl,FromDate,Paye,Martabe,Nomre1Ghabl,Nomre2Ghabl,Nomre3Ghabl,T_Sal,T_Mah,DateEntesab,K_Sal,K_Mah,G_Sal,G_Mah,SathModiriat")] E_Status e_Status)
        {
            if (ModelState.IsValid)
            {
                db.Entry(e_Status).State = EntityState.Modified;
                e_Status.Nomre1Ghabl = e_Status.Nomre1Ghabl == null ? 0 : e_Status.Nomre1Ghabl;
                e_Status.Nomre2Ghabl = e_Status.Nomre2Ghabl == null ? 0 : e_Status.Nomre2Ghabl;
                e_Status.Nomre3Ghabl = e_Status.Nomre3Ghabl == null ? 0 : e_Status.Nomre3Ghabl;
                e_Status.T_Sal = e_Status.T_Sal == null ? 0 : e_Status.T_Sal;
                e_Status.T_Mah = e_Status.T_Mah == null ? 0 : e_Status.T_Mah;
                e_Status.K_Sal = e_Status.K_Sal == null ? 0 : e_Status.K_Sal;
                e_Status.K_Mah = e_Status.K_Mah == null ? 0 : e_Status.K_Mah;
                e_Status.G_Sal = e_Status.G_Sal == null ? 0 : e_Status.G_Sal;
                e_Status.G_Mah = e_Status.G_Mah == null ? 0 : e_Status.G_Mah;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.ID = new SelectList(db.Users, "ID", "Lname", e_Status.ID);
            return View(e_Status);
        }

        // GET: E_Status/Delete/5
        public virtual ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            E_Status e_Status = db.E_Statuses.Find(id);
            if (e_Status == null)
            {
                return HttpNotFound();
            }
            return View(e_Status);
        }

        // POST: E_Status/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(long id)
        {
            E_Status e_Status = db.E_Statuses.Find(id);
            db.E_Statuses.Remove(e_Status);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
