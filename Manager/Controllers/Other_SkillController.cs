﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using Manager.Helpers;

namespace Manager.Controllers
{
    [CustomAuthorize(Roles = "User")]
    public partial class Other_SkillController : Controller
    {
        private DataBaseContext db = new DataBaseContext();

        // GET: Other_Skill
        public virtual ActionResult Index()
        {
            var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
            var other_Skills = db.Other_Skills.Include(o => o.User).Where(o => o.UserID == ID);
            return View(other_Skills.ToList());
        }

        // GET: Other_Skill/Details/5
        public virtual ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Other_Skill other_Skill = db.Other_Skills.Find(id);
            if (other_Skill == null)
            {
                return HttpNotFound();
            }
            return View(other_Skill);
        }

        // GET: Other_Skill/Create
        public virtual ActionResult Create()
        {
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname");
            return View();
        }

        // POST: Other_Skill/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "ID,Onvan_Skill,Sharh_Skill,UserID")] Other_Skill other_Skill)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                other_Skill.UserID = ID;
                db.Other_Skills.Add(other_Skill);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", other_Skill.UserID);
            return View(other_Skill);
        }

        // GET: Other_Skill/Edit/5
        public virtual ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Other_Skill other_Skill = db.Other_Skills.Find(id);
            if (other_Skill == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", other_Skill.UserID);
            return View(other_Skill);
        }

        // POST: Other_Skill/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "ID,Onvan_Skill,Sharh_Skill,UserID")] Other_Skill other_Skill)
        {
            if (ModelState.IsValid)
            {
                var ID = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                other_Skill.UserID = ID;
                db.Entry(other_Skill).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.UserID = new SelectList(db.Users, "ID", "Lname", other_Skill.UserID);
            return View(other_Skill);
        }

        // GET: Other_Skill/Delete/5
        public virtual ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Other_Skill other_Skill = db.Other_Skills.Find(id);
            if (other_Skill == null)
            {
                return HttpNotFound();
            }
            return View(other_Skill);
        }

        // POST: Other_Skill/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DeleteConfirmed(long id)
        {
            Other_Skill other_Skill = db.Other_Skills.Find(id);
            db.Other_Skills.Remove(other_Skill);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
