﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Manager
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region common configuration
            //------------------------------------------------------
            //------------------------------------------------------
            //common configuration
            //------------------------------------------------------
            //------------------------------------------------------
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/bootstrap-rtl.min_.css"
                      ));
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/customfont/fontstyle.css",
                      "~/Content/site.css",
                      "~/Content/w3.css",
                      "~/Content/w3Customize.css",
                      "~/Content/font-awesome.min.css"
                      ));

            //------------------------------------------------------
            #endregion

            #region uibootstrap configuration
            //------------------------------------------------------
            //------------------------------------------------------
            //uibootstrap configuration
            //------------------------------------------------------
            //------------------------------------------------------
            bundles.Add(new Bundle("~/Content/uibootstrapcss").Include(
                      "~/Content/uibootstrap/css/font-awesome.min.css",
                      "~/Content/uibootstrap/css/simple-line-icons.css",
                      "~/Content/uibootstrap/css/style.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/js/uibootstrapjs").Include(
                     //"~/Content/uibootstrap/js/libs/jquery.min.js",
                     //"~/Content/uibootstrap/js/libs/tether.min.js",
                     //"~/Content/uibootstrap/js/libs/bootstrap.min.js",
                     //"~/Content/uibootstrap/js/libs/pace.min.js",
                     //"~/Content/uibootstrap/js/libs/Chart.min.js",
                     "~/Content/uibootstrap/js/app.js"
                     //"~/Content/uibootstrap/js/views/main.js"
                     ));
            //------------------------------------------------------
            #endregion

            #region jquery-confirm
            bundles.Add(new ScriptBundle("~/bundles/jquery-confirmjs").Include(
                     "~/Content/jquery-confirm/jquery-confirm.js"
                     ));
            bundles.Add(new StyleBundle("~/Content/jquery-confirmcss").Include(
                     "~/Content/jquery-confirm/jquery-confirm.css"
                     ));

            #endregion


            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;

        }
    }
}