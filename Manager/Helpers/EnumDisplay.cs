﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Manager.Helpers
{
    public static class DisplayEnum
    {
        public static MvcHtmlString EnumDisplay(this HtmlHelper helper, Enum enumValue)
        {
            string enumStr;
            if (enumValue == null)
            {
                enumStr = "نامعلوم";
            }

            else if (enumValue.ToString().Equals("0"))
            {
                enumStr = "نامعلوم";
            }
            else
            {
                enumStr = enumValue.GetType()
                                  .GetMember(enumValue.ToString())
                                  .First()
                                  .GetCustomAttribute<DisplayAttribute>()
                                  .GetName();
            }
            return MvcHtmlString.Create(string.Format("<label for='{0}'>{1}</label>", enumStr, enumStr));
        }
    }
}