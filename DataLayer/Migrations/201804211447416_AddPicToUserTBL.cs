namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPicToUserTBL : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "UserPic", c => c.Binary());
            AlterColumn("dbo.Users", "Password", c => c.String(nullable: false, maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Password", c => c.String(nullable: false, maxLength: 20));
            DropColumn("dbo.Users", "UserPic");
        }
    }
}
