namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addShShToP_Info : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.P_Infos", "ShSh", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.P_Infos", "ShSh");
        }
    }
}
