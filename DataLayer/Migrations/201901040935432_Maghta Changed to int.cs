namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MaghtaChangedtoint : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.P_Infos", "ShMeli", c => c.String(nullable: false, maxLength: 80));
            AlterColumn("dbo.Edu_Recs", "Maghta", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Edu_Recs", "Maghta", c => c.String(maxLength: 250));
            DropColumn("dbo.P_Infos", "ShMeli");
        }
    }
}
