﻿using System.Data.Entity;
using DomainClass;
using DomainClass.Mapping;
namespace DataLayer
{
    public class DataBaseContext:DbContext
    {
        public DataBaseContext():base("ManagerDB")
        {
            Configuration.LazyLoadingEnabled = true;
        }
        static  DataBaseContext()
        {
            Database.SetInitializer(new DataBaseContextInitializer());
        }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<P_Info> P_Infos { get; set; }
        public DbSet<E_Status> E_Statuses { get; set; }
        public DbSet<Job_Gov> Job_Govs { get; set; }
        public DbSet<Job_NoGov> Job_NoGovs { get; set; }
        public DbSet<Job_Plan> Job_Plans { get; set; }
        public DbSet<Member_Moot> Member_Moots { get; set; }
        public DbSet<Edu_Rec> Edu_Recs { get; set; }
        public DbSet<Other_Skill> Other_Skills { get; set; }
        public DbSet<InServ_Train> InServ_Trains { get; set; }
        public DbSet<Job_Train> Job_Trains { get; set; }
        public DbSet<Pub_Train> Pub_Trains { get; set; }
        public DbSet<Res_History> Res_Historys { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Cheer> Cheers { get; set; }
        public DbSet<Degree> Degrees { get; set; }
        public DbSet<Takmili> Takmilis { get; set; }


    protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new P_InfoMap());
            modelBuilder.Configurations.Add(new E_StatusMap());
            modelBuilder.Configurations.Add(new Job_GovMap());
            modelBuilder.Configurations.Add(new Job_NoGovMap());
            modelBuilder.Configurations.Add(new Job_PlanMap());
            modelBuilder.Configurations.Add(new Member_MootMap());
            modelBuilder.Configurations.Add(new Edu_RecMap());
            modelBuilder.Configurations.Add(new Other_SkillMap());
            modelBuilder.Configurations.Add(new InServ_TrainMap());
            modelBuilder.Configurations.Add(new Job_TrainMap());
            modelBuilder.Configurations.Add(new Pub_TrainMap());
            modelBuilder.Configurations.Add(new Res_HistoryMap());
            modelBuilder.Configurations.Add(new BookMap());
            modelBuilder.Configurations.Add(new ArticleMap());
            modelBuilder.Configurations.Add(new CheerMap());
            modelBuilder.Configurations.Add(new DegreeMap());
            modelBuilder.Configurations.Add(new TakmiliMap());
        }

    }

}
