﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace MyClasses
{
    public enum Jensiat
    {
        [Display(Name = "خانم")]
        zan = 1,
        [Display(Name = "آقا")]
        mard = 2
    }
    public enum Tahol
    {
        [Display(Name = "مجرد")]
        Mojarad = 1,
        [Display(Name = "متاهل")]
        Motahel = 2,
        [Display(Name = "معیل")]
        Moel = 3,
    }
    public enum NezamVazife
    {
        [Display(Name = "پایان خدمت وظیفه")]
        Payan = 1,
        [Display(Name = "معافیت دائم")]
        Moaf = 2,
    }
    public enum NesbatShahid
    {
        [Display(Name = "پدر")]
        pedar = 1,
        [Display(Name = "مادر")]
        madar = 2,
        [Display(Name = "برادر")]
        baradar = 3,
        [Display(Name = "خواهر")]
        khahar = 4,
        [Display(Name = "فرزند")]
        farzand = 5,
        [Display(Name = "همسر")]
        hamsar = 6,
    }
    public enum NoEstekhdam
    {
        [Display(Name = "رسمی")]
        rasmi = 1,
        [Display(Name = "پیمانی")]
        peymani = 2,
        [Display(Name = "قراردادی")]
        gharardadi = 3,
        [Display(Name = "مشمول قانون کار")]
        mashmol = 4,
        [Display(Name = "سایر موارد")]
        sayer = 5,
    }
    public enum NoMoghararatEstekhdam
    {
        [Display(Name = "قانون مدیریت خدمات کشوری")]
        modiriat = 1,
        [Display(Name = "قانون کار")]
        kar = 2,
        [Display(Name = "مقررات خاص استخدام")]
        mogararatekhas = 2,
    }
    public enum VaziatKhedmat
    {
        [Display(Name = "مستخدم دستگاه")]
        mostakhdem = 1,
        [Display(Name = "مامور به خدمت")]
        mamor = 2,
        [Display(Name = "سایر")]
        sayer = 3,
    }
    public enum RotbeShoghli
    {
        [Display(Name = "مقدماتی")]
        moghadamti = 1,
        [Display(Name = "پایه")]
        paye = 2,
        [Display(Name = "ارشد")]
        arshad = 3,
        [Display(Name = "خبره")]
        khebre = 4,
        [Display(Name = "عالی")]
        ali = 5,
    }
    public enum SathModiriat
    {
        [Display(Name = "پایه")]
        paye = 1,
        [Display(Name = "میانی")]
        miani = 2,
        [Display(Name = "ارشد")]
        arshad = 3,
        [Display(Name = "عملیاتی")]
        amaliati = 4,
    }

    public enum NoHamkari
    {
        [Display(Name = "مدیر")]
        modir = 1,
        [Display(Name = "مشاور")]
        moshaver = 2,
        [Display(Name = "ناظر")]
        nazer = 3,
        [Display(Name = "مجری")]
        mojri = 4,
        [Display(Name = "کارشناس")]
        karshenas = 5,
        [Display(Name = "کارشناس ارشد")]
        karshenasarshad = 6,
    }
    public enum VaziatTarh
    {
        [Display(Name = "اجرا شده")]
        ejrashode = 1,
        [Display(Name = "اجرا نشده")]
        ejranashode = 2,
    }
    public enum NoHamkariMoot
    {
        [Display(Name = "رئیس")]
        raes = 1,
        [Display(Name = "دبیر")]
        dabir = 2,
        [Display(Name = "عضو")]
        ozv = 3,
        [Display(Name = "مدعو")]
        madov = 4,
    }

    public enum Maghta
    {
        [Display(Name = "ششم ابتدایی")]
        sheshom = 1,
        [Display(Name = "دیپلم")]
        diplom = 2,
        [Display(Name = "فوق دیپلم")]
        fogdiplom = 3,
        [Display(Name = "لیسانس")]
        lisans = 4,
        [Display(Name = "فوق لیسانس")]
        foglisans = 5,
        [Display(Name = "دکتری")]
        doktora = 6,
    }

    public enum MahalAkhz
    {
        [Display(Name = "داخل کشور")]
        dakhelkeshvar = 1,
        [Display(Name = "خارج کشور")]
        kharejkeshvar = 2,
    }
    public enum NoPajohesh
    {
        [Display(Name = "بنیادی")]
        bonyadi = 1,
        [Display(Name = "توسعه ای")]
        tose = 2,
        [Display(Name = "کاربردی")]
        karbordi = 3,
    }
    public enum NoHamkariPajohesh
    {
        [Display(Name = "مدیر")]
        modir = 1,
        [Display(Name = "مجری")]
        mojri = 2,
        [Display(Name = "مشاور")]
        moshaver = 3,
        [Display(Name = "ناظر")]
        nazer = 4,
        [Display(Name = "همکاری")]
        hamkari = 5,
    }
    public enum NoAsar
    {
        [Display(Name = "تالیف")]
        talif = 1,
        [Display(Name = "ترجمه")]
        tarjome = 2,
        [Display(Name = "گردآوری")]
        gerdavari = 3,
    }
    public enum NoMaghale
    {
        [Display(Name = "علمی پژوهشی")]
        pajoheshi = 1,
        [Display(Name = "علمی ترویجی")]
        tarviji = 2,
        [Display(Name = "چاپ شده در همایش")]
        chapshode = 3,
    }
    public enum SathTashvigh
    {
        [Display(Name = "ملی")]
        meli = 1,
        [Display(Name = "سازمانی")]
        sazmani = 2,
        [Display(Name = "داخلی")]
        dakheli = 3,
    }
    public enum NoTaghdir
    {
        [Display(Name = "عالی")]
        ali = 1,
        [Display(Name = "تخصصی")]
        takhasosi = 2,
        [Display(Name = "عمومی")]
        omomi = 3,
        [Display(Name = "دکترای افتخاری")]
        dokeftekhari = 4,
        [Display(Name = "دیپلم افتخار")]
        dipeftekhari = 5,
        [Display(Name = "لوح تقدیر")]
        lohtaghdir = 6,
        [Display(Name = "جایزه")]
        jayze = 7,
    }
    public enum TahsilatHozavi
    {
        [Display(Name = "سطح یک")]
        sath1 = 1,
        [Display(Name = "سطح دو")]
        sath2 = 2,
        [Display(Name = "سطح سه")]
        sath3 = 3,
        [Display(Name = "سطح چهار")]
        sath4 = 4,
        [Display(Name = "خارج")]
        kharej = 5,
    }

    public static class EnumExtensions
    {
        public static string GetDisplayName(this Enum enumValue)
        {
            try
            {
                return enumValue.GetType()
                                .GetMember(enumValue.ToString())
                                .First()
                                .GetCustomAttribute<DisplayAttribute>()
                                .GetName();
            }
            catch (Exception)
            {

                return "نامعلوم";
            }
        }
    }
    class MyClass
    {
    }
}
